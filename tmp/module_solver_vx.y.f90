      subroutine compute_internal_forces_order4(elt_start,elt_end)
!***********************************************************************************************************************************************************************************

        !
        !------->fill local displacement
        do k = 1,IG_NGLL        !zeta
          do l = 1,IG_NGLL     !eta
            do m = 1,IG_NGLL  !xi
! INVERSION DU PARCOURS, ITERATION SUR LES ELEMENTS EN 1ER
! VECTORISATION SUR LES ELEMENTS
              do iel = elt_start,elt_end

!ACCES NON CONTIGUES AU TABLEAU -> PAS D'OPPORTUNITE DE VECTORISATION
                igll                         = ig_hexa_gll_glonum(m,l,k,iel)
!RECOPIE DANS UN TABLEAU LOCAL AOS
!NON NECESSAIRE POUR VERSION VECTORISEE
                rl_displacement_gll(1,m,l,k) = rg_gll_displacement(1,igll)
                rl_displacement_gll(2,m,l,k) = rg_gll_displacement(2,igll)
                rl_displacement_gll(3,m,l,k) = rg_gll_displacement(3,igll)
                !
                !
                !******************************************************************************
                !->compute integrale at GLL nodes + assemble forces in global gll grid for hexa
                !******************************************************************************
                !
                !---------->derivative of displacement with respect to local coordinate xi, eta and zeta at the gll node klm

                !duxdxi = rl_displacement_gll(1,1,l,k)*rg_gll_lagrange_deriv(1,m) &
                !       + rl_displacement_gll(1,2,l,k)*rg_gll_lagrange_deriv(2,m) &
                !       + rl_displacement_gll(1,3,l,k)*rg_gll_lagrange_deriv(3,m) &
                !       + rl_displacement_gll(1,4,l,k)*rg_gll_lagrange_deriv(4,m) &
                !       + rl_displacement_gll(1,5,l,k)*rg_gll_lagrange_deriv(5,m)
!REMPLACEMENT DES ACCES AU TABLEAU LOCAL PAR DES ACCES AU TABLEAU GLOBAL
!IDEE : CREER UN VECTEUR A PARTIR DES COMPOSANTES DISPERSEES, PUIS FMA VECTORISEE
                duxdxi = rg_gll_displacement(1,ig_hexa_gll_glonum(1,l,k,iel))*rg_gll_lagrange_deriv(1,m) &
                       + rl_displacement_gll(1,2,l,k)*rg_gll_lagrange_deriv(2,m) &
                       + rl_displacement_gll(1,3,l,k)*rg_gll_lagrange_deriv(3,m) &
                       + rl_displacement_gll(1,4,l,k)*rg_gll_lagrange_deriv(4,m) &
                       + rl_displacement_gll(1,5,l,k)*rg_gll_lagrange_deriv(5,m)

                duydxi = rl_displacement_gll(2,1,l,k)*rg_gll_lagrange_deriv(1,m) &
                       + rl_displacement_gll(2,2,l,k)*rg_gll_lagrange_deriv(2,m) &
                       + rl_displacement_gll(2,3,l,k)*rg_gll_lagrange_deriv(3,m) &
                       + rl_displacement_gll(2,4,l,k)*rg_gll_lagrange_deriv(4,m) &
                       + rl_displacement_gll(2,5,l,k)*rg_gll_lagrange_deriv(5,m)

                duzdxi = rl_displacement_gll(3,1,l,k)*rg_gll_lagrange_deriv(1,m) &
                       + rl_displacement_gll(3,2,l,k)*rg_gll_lagrange_deriv(2,m) &
                       + rl_displacement_gll(3,3,l,k)*rg_gll_lagrange_deriv(3,m) &
                       + rl_displacement_gll(3,4,l,k)*rg_gll_lagrange_deriv(4,m) &
                       + rl_displacement_gll(3,5,l,k)*rg_gll_lagrange_deriv(5,m)

                duxdet = rl_displacement_gll(1,m,1,k)*rg_gll_lagrange_deriv(1,l) &
                       + rl_displacement_gll(1,m,2,k)*rg_gll_lagrange_deriv(2,l) &
                       + rl_displacement_gll(1,m,3,k)*rg_gll_lagrange_deriv(3,l) &
                       + rl_displacement_gll(1,m,4,k)*rg_gll_lagrange_deriv(4,l) &
                       + rl_displacement_gll(1,m,5,k)*rg_gll_lagrange_deriv(5,l)

               duydet = rl_displacement_gll(2,m,1,k)*rg_gll_lagrange_deriv(1,l) &
                      + rl_displacement_gll(2,m,2,k)*rg_gll_lagrange_deriv(2,l) &
                      + rl_displacement_gll(2,m,3,k)*rg_gll_lagrange_deriv(3,l) &
                      + rl_displacement_gll(2,m,4,k)*rg_gll_lagrange_deriv(4,l) &
                      + rl_displacement_gll(2,m,5,k)*rg_gll_lagrange_deriv(5,l)

               duzdet = rl_displacement_gll(3,m,1,k)*rg_gll_lagrange_deriv(1,l) &
                      + rl_displacement_gll(3,m,2,k)*rg_gll_lagrange_deriv(2,l) &
                      + rl_displacement_gll(3,m,3,k)*rg_gll_lagrange_deriv(3,l) &
                      + rl_displacement_gll(3,m,4,k)*rg_gll_lagrange_deriv(4,l) &
                      + rl_displacement_gll(3,m,5,k)*rg_gll_lagrange_deriv(5,l)

               duxdze = rl_displacement_gll(1,m,l,1)*rg_gll_lagrange_deriv(1,k) &
                      + rl_displacement_gll(1,m,l,2)*rg_gll_lagrange_deriv(2,k) &
                      + rl_displacement_gll(1,m,l,3)*rg_gll_lagrange_deriv(3,k) &
                      + rl_displacement_gll(1,m,l,4)*rg_gll_lagrange_deriv(4,k) &
                      + rl_displacement_gll(1,m,l,5)*rg_gll_lagrange_deriv(5,k)

               duydze = rl_displacement_gll(2,m,l,1)*rg_gll_lagrange_deriv(1,k) &
                      + rl_displacement_gll(2,m,l,2)*rg_gll_lagrange_deriv(2,k) &
                      + rl_displacement_gll(2,m,l,3)*rg_gll_lagrange_deriv(3,k) &
                      + rl_displacement_gll(2,m,l,4)*rg_gll_lagrange_deriv(4,k) &
                      + rl_displacement_gll(2,m,l,5)*rg_gll_lagrange_deriv(5,k)

               duzdze = rl_displacement_gll(3,m,l,1)*rg_gll_lagrange_deriv(1,k) &
                      + rl_displacement_gll(3,m,l,2)*rg_gll_lagrange_deriv(2,k) &
                      + rl_displacement_gll(3,m,l,3)*rg_gll_lagrange_deriv(3,k) &
                      + rl_displacement_gll(3,m,l,4)*rg_gll_lagrange_deriv(4,k) &
                      + rl_displacement_gll(3,m,l,5)*rg_gll_lagrange_deriv(5,k)

   !
   !---------->derivative of displacement at step n+1 with respect to global coordinate x, y and z at the gll node klm
! ACCES NON VECTORISABLES, TRANSFORMATION rg_hexa_gll_d..d.(iel, m, l, k)
               dxidx = rg_hexa_gll_dxidx(m,l,k,iel) !
               dxidy = rg_hexa_gll_dxidy(m,l,k,iel) !
               dxidz = rg_hexa_gll_dxidz(m,l,k,iel) !
               detdx = rg_hexa_gll_detdx(m,l,k,iel) !
               detdy = rg_hexa_gll_detdy(m,l,k,iel) !
               detdz = rg_hexa_gll_detdz(m,l,k,iel) !
               dzedx = rg_hexa_gll_dzedx(m,l,k,iel) !
               dzedy = rg_hexa_gll_dzedy(m,l,k,iel) !
               dzedz = rg_hexa_gll_dzedz(m,l,k,iel) !

! VECTORISABLE
               duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx !
               duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy
               duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz
               duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx
               duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy !
               duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz
               duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx
               duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy
               duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz !
   !
   !---------->compute elastic stress (elastic simulation) or unrelaxed elastic stress (viscoelastic simulation)
! ACCES NON VECTORISABLES, TRANSFORMATION rg_hexa_gll_rhovp2(iel, m, l, k)
! RESTE DU CODE VECTORISABLE
               trace_tau = (rg_hexa_gll_rhovp2(m,l,k,iel) - 2.0*rg_hexa_gll_rhovs2(m,l,k,iel))*(duxdx+duydy+duzdz)
               tauxx     = trace_tau + 2.0*rg_hexa_gll_rhovs2(m,l,k,iel)*duxdx
               tauyy     = trace_tau + 2.0*rg_hexa_gll_rhovs2(m,l,k,iel)*duydy
               tauzz     = trace_tau + 2.0*rg_hexa_gll_rhovs2(m,l,k,iel)*duzdz
               tauxy     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdy+duydx)
               tauxz     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdz+duzdx)
               tauyz     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duydz+duzdy)
   !
   !---------->store members of integration of the gll node klm
! ACCES NON VECTORISABLES, TRANSFORMATION rg_hexa_gll_jacobian_det(iel, m, l, k)
! RESTE DU CODE VECTORISABLE
               intpx1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz)
               intpx2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*detdx+tauxy*detdy+tauxz*detdz)
               intpx3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz)

               intpy1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz)
               intpy2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*detdx+tauyy*detdy+tauyz*detdz)
               intpy3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz)

               intpz1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz)
               intpz2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*detdx+tauyz*detdy+tauzz*detdz)
               intpz3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz)

                  enddo !xi
               enddo    !eta
            enddo       !zeta

         !
         !->finish integration for hexa (internal forces at step n+1)
            do k = 1,IG_NGLL
               do l = 1,IG_NGLL
                  do m = 1,IG_NGLL

                     tmpx1 = intpx1(1,l,k)*rg_gll_lagrange_deriv(m,1)*rg_gll_weight(1) &
                           + intpx1(2,l,k)*rg_gll_lagrange_deriv(m,2)*rg_gll_weight(2) &
                           + intpx1(3,l,k)*rg_gll_lagrange_deriv(m,3)*rg_gll_weight(3) &
                           + intpx1(4,l,k)*rg_gll_lagrange_deriv(m,4)*rg_gll_weight(4) &
                           + intpx1(5,l,k)*rg_gll_lagrange_deriv(m,5)*rg_gll_weight(5)

                     tmpy1 = intpy1(1,l,k)*rg_gll_lagrange_deriv(m,1)*rg_gll_weight(1) &
                           + intpy1(2,l,k)*rg_gll_lagrange_deriv(m,2)*rg_gll_weight(2) &
                           + intpy1(3,l,k)*rg_gll_lagrange_deriv(m,3)*rg_gll_weight(3) &
                           + intpy1(4,l,k)*rg_gll_lagrange_deriv(m,4)*rg_gll_weight(4) &
                           + intpy1(5,l,k)*rg_gll_lagrange_deriv(m,5)*rg_gll_weight(5)

                     tmpz1 = intpz1(1,l,k)*rg_gll_lagrange_deriv(m,1)*rg_gll_weight(1) &
                           + intpz1(2,l,k)*rg_gll_lagrange_deriv(m,2)*rg_gll_weight(2) &
                           + intpz1(3,l,k)*rg_gll_lagrange_deriv(m,3)*rg_gll_weight(3) &
                           + intpz1(4,l,k)*rg_gll_lagrange_deriv(m,4)*rg_gll_weight(4) &
                           + intpz1(5,l,k)*rg_gll_lagrange_deriv(m,5)*rg_gll_weight(5)

                     tmpx2 = intpx2(m,1,k)*rg_gll_lagrange_deriv(l,1)*rg_gll_weight(1) &
                           + intpx2(m,2,k)*rg_gll_lagrange_deriv(l,2)*rg_gll_weight(2) &
                           + intpx2(m,3,k)*rg_gll_lagrange_deriv(l,3)*rg_gll_weight(3) &
                           + intpx2(m,4,k)*rg_gll_lagrange_deriv(l,4)*rg_gll_weight(4) &
                           + intpx2(m,5,k)*rg_gll_lagrange_deriv(l,5)*rg_gll_weight(5)

                     tmpy2 = intpy2(m,1,k)*rg_gll_lagrange_deriv(l,1)*rg_gll_weight(1) &
                           + intpy2(m,2,k)*rg_gll_lagrange_deriv(l,2)*rg_gll_weight(2) &
                           + intpy2(m,3,k)*rg_gll_lagrange_deriv(l,3)*rg_gll_weight(3) &
                           + intpy2(m,4,k)*rg_gll_lagrange_deriv(l,4)*rg_gll_weight(4) &
                           + intpy2(m,5,k)*rg_gll_lagrange_deriv(l,5)*rg_gll_weight(5)

                     tmpz2 = intpz2(m,1,k)*rg_gll_lagrange_deriv(l,1)*rg_gll_weight(1) &
                           + intpz2(m,2,k)*rg_gll_lagrange_deriv(l,2)*rg_gll_weight(2) &
                           + intpz2(m,3,k)*rg_gll_lagrange_deriv(l,3)*rg_gll_weight(3) &
                           + intpz2(m,4,k)*rg_gll_lagrange_deriv(l,4)*rg_gll_weight(4) &
                           + intpz2(m,5,k)*rg_gll_lagrange_deriv(l,5)*rg_gll_weight(5)

                     tmpx3 = intpx3(m,l,1)*rg_gll_lagrange_deriv(k,1)*rg_gll_weight(1) &
                           + intpx3(m,l,2)*rg_gll_lagrange_deriv(k,2)*rg_gll_weight(2) &
                           + intpx3(m,l,3)*rg_gll_lagrange_deriv(k,3)*rg_gll_weight(3) &
                           + intpx3(m,l,4)*rg_gll_lagrange_deriv(k,4)*rg_gll_weight(4) &
                           + intpx3(m,l,5)*rg_gll_lagrange_deriv(k,5)*rg_gll_weight(5)

                     tmpy3 = intpy3(m,l,1)*rg_gll_lagrange_deriv(k,1)*rg_gll_weight(1) &
                           + intpy3(m,l,2)*rg_gll_lagrange_deriv(k,2)*rg_gll_weight(2) &
                           + intpy3(m,l,3)*rg_gll_lagrange_deriv(k,3)*rg_gll_weight(3) &
                           + intpy3(m,l,4)*rg_gll_lagrange_deriv(k,4)*rg_gll_weight(4) &
                           + intpy3(m,l,5)*rg_gll_lagrange_deriv(k,5)*rg_gll_weight(5)

                     tmpz3 = intpz3(m,l,1)*rg_gll_lagrange_deriv(k,1)*rg_gll_weight(1) &
                           + intpz3(m,l,2)*rg_gll_lagrange_deriv(k,2)*rg_gll_weight(2) &
                           + intpz3(m,l,3)*rg_gll_lagrange_deriv(k,3)*rg_gll_weight(3) &
                           + intpz3(m,l,4)*rg_gll_lagrange_deriv(k,4)*rg_gll_weight(4) &
                           + intpz3(m,l,5)*rg_gll_lagrange_deriv(k,5)*rg_gll_weight(5)

                     fac1 = rg_gll_weight(l)*rg_gll_weight(k)
                     fac2 = rg_gll_weight(m)*rg_gll_weight(k)
                     fac3 = rg_gll_weight(m)*rg_gll_weight(l)

                     rl_acceleration_gll(1,m,l,k) = (fac1*tmpx1 + fac2*tmpx2 + fac3*tmpx3)
                     rl_acceleration_gll(2,m,l,k) = (fac1*tmpy1 + fac2*tmpy2 + fac3*tmpy3)
                     rl_acceleration_gll(3,m,l,k) = (fac1*tmpz1 + fac2*tmpz2 + fac3*tmpz3)

                  enddo
               enddo
            enddo

            do k = 1,IG_NGLL        !zeta
               do l = 1,IG_NGLL     !eta
                  do m = 1,IG_NGLL  !xi

                     igll                        = ig_hexa_gll_glonum(m,l,k,iel)

                     rg_gll_acceleration(1,igll) = rg_gll_acceleration(1,igll) - rl_acceleration_gll(1,m,l,k)
                     rg_gll_acceleration(2,igll) = rg_gll_acceleration(2,igll) - rl_acceleration_gll(2,m,l,k)
                     rg_gll_acceleration(3,igll) = rg_gll_acceleration(3,igll) - rl_acceleration_gll(3,m,l,k)

                  enddo
               enddo
            enddo

         enddo !loop on hexahedron elements

         return
!***********************************************************************************************************************************************************************************
      end subroutine compute_internal_forces_order4

end module mod_solver
