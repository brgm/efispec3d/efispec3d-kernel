#include <cstddef>

#include <immintrin.h>


#define IDX2( m, l ) ( 5 * l + m )
#define IDX3( m, l, k ) ( 25 * k + 5 * l + m )
#define IDX4( m, l, k, iel ) ( 125 * iel + 25 * k + 5 * l + m )

// Global arrays

std::size_t * ig_hexa_gll_glonum; // 5 * 5 * 5 * elements

float * rg_gll_displacement;      // 3 coords * elements

float * rg_gll_lagrange_deriv;    // 5 * 5

float * rg_hexa_gll_dxidx;        // 5 * 5 * 5 * elements
float * rg_hexa_gll_detdx;
float * rg_hexa_gll_dzedx;

float * rg_hexa_gll_dxidy;
float * rg_hexa_gll_detdy;
float * rg_hexa_gll_dzedy;

float * rg_hexa_gll_dxidz;
float * rg_hexa_gll_detdz;
float * rg_hexa_gll_dzedz;

float * rg_hexa_gll_rhovp2;       // 5 * 5 * 5 * elements
float * rg_hexa_gll_rhovs2;       // 5 * 5 * 5 * elements

float * rg_hexa_gll_jacobian_det; // 5 * 5 * 5 * elements

float * rg_gll_weight;            // 5

float * rg_gll_acceleration;      // 3 coords * elements


//

void compute_internal_forces_order4( std::size_t elt_start, std::size_t elt_end )
{
  // Allocate all the local arrays at once ( 5 * 5 * 5 * 9 * 32 = 36 kB ).
  __m256 * local = new __m256[ 5 * 5 * 5 * 9 ];

  __m256 * intpx1 = &local[    0 ];
  __m256 * intpy1 = &local[  125 ];
  __m256 * intpz1 = &local[  250 ];

  __m256 * intpx2 = &local[  375 ];
  __m256 * intpy2 = &local[  500 ];
  __m256 * intpz2 = &local[  625 ];

  __m256 * intpx3 = &local[  750 ];
  __m256 * intpy3 = &local[  875 ];
  __m256 * intpz3 = &local[ 1000 ];

  __m256 coeff;
  __m256 duxdx, duydx, duzdx
       , duxdy, duydy, duzdy
       , duxdz, duydz, duzdz
       , duxdet, duydet, duzdet
       , duxdze, duydze, duzdze
       , detdx, detdy, detdz;

  // Elements are processed by vectors of 8.
  for( std::size_t iel = elt_start ; iel < elt_end ; iel += 8 )
  {
    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          //
          auto r0x = _mm256_setr_ps( rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 0 ) ] - 1 ]
                                   , rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 1 ) ] - 1 ]
                                   , rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 2 ) ] - 1 ]
                                   , rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 3 ) ] - 1 ]
                                   , rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 4 ) ] - 1 ]
                                   , rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 5 ) ] - 1 ]
                                   , rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 6 ) ] - 1 ]
                                   , rg_gll_displacement[ 0, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 7 ) ] - 1 ] );
          auto r0y = _mm256_setr_ps( rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 0 ) ] - 1 ]
                                   , rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 1 ) ] - 1 ]
                                   , rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 2 ) ] - 1 ]
                                   , rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 3 ) ] - 1 ]
                                   , rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 4 ) ] - 1 ]
                                   , rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 5 ) ] - 1 ]
                                   , rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 6 ) ] - 1 ]
                                   , rg_gll_displacement[ 1, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 7 ) ] - 1 ] );
          auto r0z = _mm256_setr_ps( rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 0 ) ] - 1 ]
                                   , rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 1 ) ] - 1 ]
                                   , rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 2 ) ] - 1 ]
                                   , rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 3 ) ] - 1 ]
                                   , rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 4 ) ] - 1 ]
                                   , rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 5 ) ] - 1 ]
                                   , rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 6 ) ] - 1 ]
                                   , rg_gll_displacement[ 2, ig_hexa_gll_glonum[ IDX4( m, l, k, iel + 7 ) ] - 1 ] );
          //

          coeff = _mm256_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, m ) ] );

          auto duxdxi = r0x * coeff;
          auto duydxi = r0y * coeff;
          auto duzdxi = r0y * coeff;

          coeff = _mm256_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, m ) ] );

          // ...

          //
          auto dxidx = _mm256_load_ps( &rg_hexa_gll_dxidx[ IDX4( m, l, k, iel ) ] );
          auto detdx = _mm256_load_ps( &rg_hexa_gll_detdx[ IDX4( m, l, k, iel ) ] );
          auto dzedx = _mm256_load_ps( &rg_hexa_gll_dzedx[ IDX4( m, l, k, iel ) ] );

          duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx;
          duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx;
          duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx;

          auto dxidy = _mm256_load_ps( &rg_hexa_gll_dxidy[ IDX4( m, l, k, iel ) ] );
          auto detdy = _mm256_load_ps( &rg_hexa_gll_detdy[ IDX4( m, l, k, iel ) ] );
          auto dzedy = _mm256_load_ps( &rg_hexa_gll_dzedy[ IDX4( m, l, k, iel ) ] );

          duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy;
          duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy;
          duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy;

          auto dxidz = _mm256_load_ps( &rg_hexa_gll_dxidz[ IDX4( m, l, k, iel ) ] );
          auto detdz = _mm256_load_ps( &rg_hexa_gll_detdz[ IDX4( m, l, k, iel ) ] );
          auto dzedz = _mm256_load_ps( &rg_hexa_gll_dzedz[ IDX4( m, l, k, iel ) ] );

          duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz;
          duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz;
          duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz;

          // Load 8 values from rg_hexa_gll_rhovp2 and rg_hexa_gll_rhovs2.
          auto rhovp2 = _mm256_load_ps( &rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel ) ] );
          auto rhovs2 = _mm256_load_ps( &rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel ) ] );

          auto trace_tau = ( rhovp2 - 2.0 * rhovs2 )*(duxdx+duydy+duzdz);
          auto tauxx     = trace_tau + 2.0*rhovs2*duxdx;
          auto tauyy     = trace_tau + 2.0*rhovs2*duydy;
          auto tauzz     = trace_tau + 2.0*rhovs2*duzdz;
          auto tauxy     =                 rhovs2*(duxdy+duydx);
          auto tauxz     =                 rhovs2*(duxdz+duzdx);
          auto tauyz     =                 rhovs2*(duydz+duzdy);

          intpx1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
          intpx2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*detdx+tauxy*detdy+tauxz*detdz);
          intpx3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

          intpy1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
          intpy2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*detdx+tauyy*detdy+tauyz*detdz);
          intpy3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

          intpz1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
          intpz2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*detdx+tauyz*detdy+tauzz*detdz);
          intpz3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
        }
      }
    }

    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          auto tmpx1 = intpx1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpx1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpx1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpx1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpx1[ IDX3( 5, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpy1 = intpy1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpy1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpy1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpy1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpy1[ IDX3( 5, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpz1 = intpz1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpz1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpz1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpz1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpz1[ IDX3( 5, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpx2 = intpx2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpx2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpx2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpx2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpx2[ IDX3( m, 5, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpy2 = intpy2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpy2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpy2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpy2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpy2[ IDX3( m, 5, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpz2 = intpz2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpz2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpz2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpz2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpz2[ IDX3( m, 5, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpx3 = intpx3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpx3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpx3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpx3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpx3[ IDX3( m, l, 5 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpy3 = intpy3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpy3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpy3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpy3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpy3[ IDX3( m, l, 5 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 5 ) ]*rg_gll_weight[ 5 ];

          auto tmpz3 = intpz3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpz3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpz3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpz3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 4 ) ]*rg_gll_weight[ 4 ]
                     + intpz3[ IDX3( m, l, 5 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 5 ) ]*rg_gll_weight[ 5 ];

          auto fac1 = rg_gll_weight[ l ]*rg_gll_weight[ k ];
          auto fac2 = rg_gll_weight[ m ]*rg_gll_weight[ k ];
          auto fac3 = rg_gll_weight[ m ]*rg_gll_weight[ l ];

          auto tmpx = (fac1*tmpx1 + fac2*tmpx2 + fac3*tmpx3);
          auto tmpy = (fac1*tmpy1 + fac2*tmpy2 + fac3*tmpy3);
          auto tmpz = (fac1*tmpz1 + fac2*tmpz2 + fac3*tmpz3);

          // Store results.
          for( std::size_t i = 0 ; i < 8 ; ++i )
          {
            rg_gll_acceleration[ ig_hexa_gll_glonum[ IDX4( m, l, k, iel + i ) ] * 3     ] -= tmpx[ i ];
            rg_gll_acceleration[ ig_hexa_gll_glonum[ IDX4( m, l, k, iel + i ) ] * 3 + 1 ] -= tmpy[ i ];
            rg_gll_acceleration[ ig_hexa_gll_glonum[ IDX4( m, l, k, iel + i ) ] * 3 + 2 ] -= tmpz[ i ];
          }
        }
      }
    }

  }

}
