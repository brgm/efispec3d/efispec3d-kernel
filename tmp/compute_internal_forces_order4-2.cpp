
igll = ig_hexa_gll_glonum(m,l,k,iel);

rl_displacement_gll(1,m,l,k) = rg_gll_displacement(1,igll);
rl_displacement_gll(2,m,l,k) = rg_gll_displacement(2,igll);
rl_displacement_gll(3,m,l,k) = rg_gll_displacement(3,igll);

//////////////////:

iel=2;
k=l=m=1;

duxdxi = rg_gll_displacement(1,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(2,1,1,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(3,1,1,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(4,1,1,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(5,1,1,2)) * rg_gll_lagrange_deriv(5,1);

duydxi = rg_gll_displacement(2,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(2,1,1,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(3,1,1,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(4,1,1,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(5,1,1,2)) * rg_gll_lagrange_deriv(5,1);

duzdxi = rg_gll_displacement(3,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(2,1,1,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(3,1,1,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(4,1,1,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(5,1,1,2)) * rg_gll_lagrange_deriv(5,1);


duxdet = rg_gll_displacement(1,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,2,1,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,3,1,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,4,1,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,5,1,2)) * rg_gll_lagrange_deriv(5,1);

duydet = rg_gll_displacement(2,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,2,1,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,3,1,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,4,1,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,5,1,2)) * rg_gll_lagrange_deriv(5,1);

duzdet = rg_gll_displacement(3,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,2,1,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,3,1,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,4,1,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,5,1,2)) * rg_gll_lagrange_deriv(5,1);


duxdze = rg_gll_displacement(1,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,1,2,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,1,3,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,1,4,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(1,ig_hexa_gll_glonum(1,1,5,2)) * rg_gll_lagrange_deriv(5,1);

duydze = rg_gll_displacement(2,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,1,2,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,1,3,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,1,4,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(2,ig_hexa_gll_glonum(1,1,5,2)) * rg_gll_lagrange_deriv(5,1);

duzdze = rg_gll_displacement(3,ig_hexa_gll_glonum(1,1,1,2)) * rg_gll_lagrange_deriv(1,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,1,2,2)) * rg_gll_lagrange_deriv(2,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,1,3,2)) * rg_gll_lagrange_deriv(3,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,1,4,2)) * rg_gll_lagrange_deriv(4,1)
       + rg_gll_displacement(3,ig_hexa_gll_glonum(1,1,5,2)) * rg_gll_lagrange_deriv(5,1);


dxidx = rg_hexa_gll_dxidx(1,1,1,2);
dxidy = rg_hexa_gll_dxidy(1,1,1,2);
dxidz = rg_hexa_gll_dxidz(1,1,1,2);
detdx = rg_hexa_gll_detdx(1,1,1,2);
detdy = rg_hexa_gll_detdy(1,1,1,2);
detdz = rg_hexa_gll_detdz(1,1,1,2);
dzedx = rg_hexa_gll_dzedx(1,1,1,2);
dzedy = rg_hexa_gll_dzedy(1,1,1,2);
dzedz = rg_hexa_gll_dzedz(1,1,1,2);


duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx;
duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy;
duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz;
duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx;
duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy;
duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz;
duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx;
duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy;
duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz;


trace_tau = (rg_hexa_gll_rhovp2(1,1,1,2) - 2.0*rg_hexa_gll_rhovs2(1,1,1,2))*(duxdx+duydy+duzdz);
tauxx     = trace_tau + 2.0*rg_hexa_gll_rhovs2(1,1,1,2)*duxdx;
tauyy     = trace_tau + 2.0*rg_hexa_gll_rhovs2(1,1,1,2)*duydy;
tauzz     = trace_tau + 2.0*rg_hexa_gll_rhovs2(1,1,1,2)*duzdz;
tauxy     =                 rg_hexa_gll_rhovs2(1,1,1,2)*(duxdy+duydx);
tauxz     =                 rg_hexa_gll_rhovs2(1,1,1,2)*(duxdz+duzdx);
tauyz     =                 rg_hexa_gll_rhovs2(1,1,1,2)*(duydz+duzdy);


intpx1(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
intpx2(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxx*detdx+tauxy*detdy+tauxz*detdz);
intpx3(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

intpy1(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
intpy2(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxy*detdx+tauyy*detdy+tauyz*detdz);
intpy3(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

intpz1(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
intpz2(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxz*detdx+tauyz*detdy+tauzz*detdz);
intpz3(1,1,1) = rg_hexa_gll_jacobian_det(1,1,1,2)*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);

/////////////////////////////////////


tmpx1 = intpx1(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpx1(2,1,1)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpx1(3,1,1)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpx1(4,1,1)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpx1(5,1,1)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpy1 = intpy1(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpy1(2,1,1)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpy1(3,1,1)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpy1(4,1,1)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpy1(5,1,1)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpz1 = intpz1(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpz1(2,1,1)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpz1(3,1,1)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpz1(4,1,1)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpz1(5,1,1)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpx2 = intpx2(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpx2(1,2,1)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpx2(1,3,1)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpx2(1,4,1)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpx2(1,5,1)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpy2 = intpy2(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpy2(1,2,1)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpy2(1,3,1)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpy2(1,4,1)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpy2(1,5,1)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpz2 = intpz2(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpz2(1,2,1)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpz2(1,3,1)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpz2(1,4,1)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpz2(1,5,1)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpx3 = intpx3(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpx3(1,1,2)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpx3(1,1,3)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpx3(1,1,4)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpx3(1,1,5)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpy3 = intpy3(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpy3(1,1,2)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpy3(1,1,3)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpy3(1,1,4)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpy3(1,1,5)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

tmpz3 = intpz3(1,1,1)*rg_gll_lagrange_deriv(1,1)*rg_gll_weight(1)
      + intpz3(1,1,2)*rg_gll_lagrange_deriv(1,2)*rg_gll_weight(2)
      + intpz3(1,1,3)*rg_gll_lagrange_deriv(1,3)*rg_gll_weight(3)
      + intpz3(1,1,4)*rg_gll_lagrange_deriv(1,4)*rg_gll_weight(4)
      + intpz3(1,1,5)*rg_gll_lagrange_deriv(1,5)*rg_gll_weight(5);

fac1 = rg_gll_weight(1)*rg_gll_weight(1);
fac2 = rg_gll_weight(1)*rg_gll_weight(1);
fac3 = rg_gll_weight(1)*rg_gll_weight(1);

rl_acceleration_gll(1,1,1,1) = (fac1*tmpx1 + fac2*tmpx2 + fac3*tmpx3);
rl_acceleration_gll(2,1,1,1) = (fac1*tmpy1 + fac2*tmpy2 + fac3*tmpy3);
rl_acceleration_gll(3,1,1,1) = (fac1*tmpz1 + fac2*tmpz2 + fac3*tmpz3);


//////////////////////////





