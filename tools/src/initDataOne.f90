program initDataOne

   implicit none

   real, parameter                    :: fixedValue = 1.0e0
   real                               :: v

   integer, dimension(:), allocatable :: d
   integer                            :: dims

   character(255)                     :: f
   character(30)                      :: path = "../data/Elast40000"

   interface

      subroutine readDims(f,d)

         character(len=*), intent( in)                            :: f
         integer         , intent(out), dimension(:), allocatable :: d

      end subroutine


      subroutine writeData(f,d,v)
      
         character(len=*), intent(in)                :: f
         integer         , intent(in), dimension(:)  :: d
         real            , intent(in)                :: v
      
      end subroutine


   end interface

   v = fixedValue

!
!->rg_gll_displacement.dat

   f = trim(path)//"/rg_gll_displacement.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_gll_displacement-one.dat"
   call writeData(f,d,v)

!
!->rg_gll_lagrange_deriv.dat

   f = trim(path)//"/rg_gll_lagrange_deriv.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_gll_lagrange_deriv-one.dat"
   call writeData(f,d,v)
 
!
!->rg_hexa_gll_dxidx.dat

   f = trim(path)//"/rg_hexa_gll_dxidx.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_dxidx-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_dxidy.dat

   f = trim(path)//"/rg_hexa_gll_dxidy.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_dxidy-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_dxidz.dat

   f = trim(path)//"/rg_hexa_gll_dxidz.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_dxidz-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_detdx.dat

   f = trim(path)//"/rg_hexa_gll_detdx.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_detdx-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_detdy.dat

   f = trim(path)//"/rg_hexa_gll_detdy.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_detdy-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_detdz.dat

   f = trim(path)//"/rg_hexa_gll_detdz.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_detdz-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_dzedx.dat

   f = trim(path)//"/rg_hexa_gll_dzedx.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_dzedx-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_dzedy.dat

   f = trim(path)//"/rg_hexa_gll_dzedy.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_dzedy-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_dzedz.dat

   f = trim(path)//"/rg_hexa_gll_dzedz.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_dzedz-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_rhovp2.dat

   f = trim(path)//"/rg_hexa_gll_rhovp2.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_rhovp2-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_rhovs2.dat

   f = trim(path)//"/rg_hexa_gll_rhovs2.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_rhovs2-one.dat"
   call writeData(f,d,v)

!
!->rg_hexa_gll_jacobian_det.dat

   f = trim(path)//"/rg_hexa_gll_jacobian_det.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_hexa_gll_jacobian_det-one.dat"
   call writeData(f,d,v)

!
!->rg_gll_weight.dat

   f = trim(path)//"/rg_gll_weight.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_gll_weight-one.dat"
   call writeData(f,d,v)

!
!->rg_gll_acceleration_before_loop_iel.dat
 
   f = trim(path)//"/rg_gll_acceleration_before_loop_iel.dat"
   call readDims(f,d)

   f = trim(path)//"/rg_gll_acceleration_before_loop_iel-one.dat"
   call writeData(f,d,v)

   stop

end program

subroutine readDims(f,d)

   implicit none

   character(len=*), intent( in)                            :: f
   integer         , intent(out), dimension(:), allocatable :: d

   integer                                                 :: dims
   integer                                                 :: fileid

   open(newunit=fileid,file=trim(f),action="read",access="stream",form="unformatted")

   read(fileid) dims

   allocate(d(dims))
 
   read(fileid) d(:)
   
   close(fileid)

   print *,trim(f)," dims, d(:) = ",dims,d(:)

   return

end subroutine  readDims


subroutine writeData(f,d,v)

   implicit none

   character(len=*), intent(in)                :: f
   integer         , intent(in), dimension(:)  :: d
   real            , intent(in)                :: v

   real            , dimension(:), allocatable :: a 
   
   integer                                     :: p
   integer                                     :: i
   integer                                     :: fileid

   p = 1

   do i = 1,size(d)

      p = p*d(i)

   enddo

!
!->write value 'v' 'size(d)' times

   open(newunit=fileid,file=trim(f),action="write",access="stream",form="unformatted")

   allocate(a(p))

   a(:) = v

   write(fileid) rank(d),d(:),a(:)

   close(fileid)

   return

end subroutine
