#include <iostream>
#include <fstream>
#include <vector>

#include <boost/align/aligned_allocator.hpp>


void soa2hyb( std::size_t interleave
            , std::string const & path
            , std::string const & in0
            , std::string const & in1
            , std::string const & in2
            , std::string const & out )
{
  uint32_t tmp0, tmp1, tmp2;
  uint32_t dims;

  std::vector< float > vin0;
  std::vector< float > vin1;
  std::vector< float > vin2;
  std::vector< float > vout;
  std::vector< std::size_t > vdims;

  std::ifstream ifs0( path + "/" + in0, std::ios_base::in
                                      | std::ios_base::binary );
  std::ifstream ifs1( path + "/" + in1, std::ios_base::in
                                      | std::ios_base::binary );
  std::ifstream ifs2( path + "/" + in2, std::ios_base::in
                                      | std::ios_base::binary );
  std::ofstream ofs( path+ "/" + out, std::ios_base::out
                                    | std::ios_base::binary );

  std::cout << "ifs0=" << path+"/"+ in0 << std::endl;

  ifs0.read( reinterpret_cast<char*>( &tmp0 ), sizeof( uint32_t ) );
  ifs1.read( reinterpret_cast<char*>( &tmp1 ), sizeof( uint32_t ) );
  ifs2.read( reinterpret_cast<char*>( &tmp2 ), sizeof( uint32_t ) );

  dims = tmp0;

  std::cout << "dims=" << dims << std::endl;

  ofs.write( reinterpret_cast<char*>( &dims ), sizeof( uint32_t ) );

  vdims.resize( dims );

  std::size_t size = 1;
  for( uint32_t i = 0 ; i < dims ; ++i )
  {
    uint32_t dim;
    ifs0.read( reinterpret_cast<char*>( & dim ), sizeof( uint32_t ) );
    ifs1.read( reinterpret_cast<char*>( & dim ), sizeof( uint32_t ) );
    ifs2.read( reinterpret_cast<char*>( & dim ), sizeof( uint32_t ) );
    vdims[ i ] = dim;
    size *= dim;
  }
  for( uint32_t i = 0 ; i < dims ; ++i )
  {
    std::cout << vdims[ i ] << std::endl;
    ofs.write( reinterpret_cast<char*>( &vdims[ (i + dims ) % dims ] ), sizeof( uint32_t ) );
  }

  // interleave
  ofs.write( reinterpret_cast<char*>( &interleave ), sizeof( uint32_t ) );


  vin0.resize( size );
  vin1.resize( size );
  vin2.resize( size );

  vout.resize( 3 * ( ( size - 1 ) / interleave + 1 ) * interleave );

  ifs0.read( reinterpret_cast<char*>( vin0.data() ), size * sizeof( float ) );
  ifs1.read( reinterpret_cast<char*>( vin1.data() ), size * sizeof( float ) );
  ifs2.read( reinterpret_cast<char*>( vin2.data() ), size * sizeof( float ) );

/*
  for( std::size_t k = 0 ; k < vdims[ 0 ] ; ++k )
  {
    for( std::size_t l = 0 ; l < vdims[ 1 ] ; ++l )
    {
      for( std::size_t m = 0 ; m < vdims[ 2 ] ; ++m )
      {
        for( std::size_t iel = 0 ; iel < vdims[ 3 ] ; ++iel )
        {
          vout[ vdims[3] *( m + l * 5 + k * 25 ) + iel ] = vin[ m + l * 5 + k * 25 + iel * 125 ];
        }
      }
    }
  }
*/
  for( std::size_t iel = 0 ; iel < vdims[ 3 ] ; iel += interleave )
  {
    for( std::size_t k = 0 ; k < vdims[ 0 ] ; ++k )
    {
      for( std::size_t l = 0 ; l < vdims[ 1 ] ; ++l )
      {
        for( std::size_t m = 0 ; m < vdims[ 2 ] ; ++m )
        {
          for( std::size_t i = 0 ; i < interleave ; ++i )
          {
            vout[ 3 * ( iel * 125 + interleave * ( m + l * 5 + k * 25 ) ) + i                  ] = vin0[ m + l * 5 + k * 25 + ( iel + i ) * 125 ];
            vout[ 3 * ( iel * 125 + interleave * ( m + l * 5 + k * 25 ) ) + i +     interleave ] = vin1[ m + l * 5 + k * 25 + ( iel + i ) * 125 ];
            vout[ 3 * ( iel * 125 + interleave * ( m + l * 5 + k * 25 ) ) + i + 2 * interleave ] = vin2[ m + l * 5 + k * 25 + ( iel + i ) * 125 ];

          }
        }
      }
    }
  }

  ofs.write( reinterpret_cast<char*>( vout.data() ), vout.size() * sizeof( float ) );

}



int main( int argc, char * argv[] )
{
  if( argc > 1 )
  {
    std::string path = argv[ 1 ];

    soa2hyb( 8, path, "rg_hexa_gll_dxidx.dat", "rg_hexa_gll_dxidy.dat", "rg_hexa_gll_dxidz.dat", "rg_hexa_gll_dxi-hyb.dat");
    soa2hyb( 8, path, "rg_hexa_gll_detdx.dat", "rg_hexa_gll_detdy.dat", "rg_hexa_gll_detdz.dat", "rg_hexa_gll_det-hyb.dat");
    soa2hyb( 8, path, "rg_hexa_gll_dzedx.dat", "rg_hexa_gll_dzedy.dat", "rg_hexa_gll_dzedz.dat", "rg_hexa_gll_dze-hyb.dat");
  }
  else
  {
    std::cerr << "Error: provide the path to the files to convert." << std::endl;
    return 1;
  }

  return 0;
}
