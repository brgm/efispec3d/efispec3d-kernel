#include <iostream>
#include <fstream>
#include <vector>


int main()
{
  std::ifstream ifs( "../data/Elast40000/ig_hexa_gll_glonum-C.dat");

  std::ofstream ofs( "../data/Elast40000/ig_hexa_gll_glonum-C-soa.dat");

  std::vector< uint32_t > vin, vout;

  std::uint32_t nbdims;

  std::vector< uint32_t > dims;

  ifs.read( (char*)&nbdims, sizeof( std::uint32_t ) );
  ofs.write( (char*)&nbdims, sizeof( std::uint32_t ) );

  dims.resize( nbdims );

  std::size_t size = 1;
  for( std::uint32_t i = 0 ; i < dims.size() ; ++i )
  {
    ifs.read( (char *)&dims[ i ], sizeof( std::uint32_t ) );
    ofs.write( (char *)&dims[ i ], sizeof( std::uint32_t ) );
    std::cout << dims[ i ] << std::endl;
    size *= dims[ i ];
  }

  vin.resize( size );
  vout.resize( size );

  ifs.read( (char *)vin.data(), size * sizeof( std::uint32_t ) );

  std::cout << "debug: start\n";
  //
  for( std::size_t j = 0 ; j < 125 ; ++j )
  {
    for( std::size_t i = 0 ; i < dims[ 3 ] ; ++i )
    {
      vout[ j * dims[ 3 ] + i ] = vin[ i * 125 + j ];
      if( i == 0 )
      {
        std::cout << vin[ i * 125 + j ] << std::endl;
      }
    }
  }

  std::cout << "debug: stop\n";

  for( std::size_t i = 0 ; i < vout.size() ; i+= dims[3])
  {
    std::cout << vout[ i ] << std::endl;
  }

  ofs.write( (char *)vout.data(), size * sizeof( std::uint32_t ) );

  ifs.close();

  ofs.close();

  return 0;
}
