#include <iostream>
#include <fstream>
#include <cmath>


int main( int argc, char * argv[] )
{
  float f0, f1;
  std::size_t i = 0;

  if( argc != 3 )
  {
    std::cerr << "Error: Invalid number of arguments." << std::endl;
    return 1;
  }
  std::ifstream file0( argv[ 1 ], std::ios_base::in | std::ios_base::binary );
  std::ifstream file1( argv[ 2 ], std::ios_base::in | std::ios_base::binary );

  auto m = 0.0f;
  auto M = 0.0f;
  auto moy = 0.0f;
  auto nb_diff = 0;

  while( file0.read( reinterpret_cast< char * >( &f0 ), sizeof( float ) ) && file1.read( reinterpret_cast< char * >( &f1 ), sizeof( float ) ) )
  {
    float absdif = fabsf( f0 - f1 );
    m = std::min( m, absdif );
    M = std::max( M, absdif );
    if( absdif != 0) {
      moy += absdif;
      nb_diff++;
    }
    
    // if( absdif > fabsf(f0)/100.0f )
    // {
    auto relative_err = 0.0f;

    if(f0 != 0.0f) {
      relative_err = absdif/fabsf(f0);
    }
    
    std::cout << i << ' ' << f0 << ' ' << f1 << ' ' << absdif << ' ' << relative_err << '\n';
    // }

    ++i;
  }
  moy /= nb_diff;

  // std::cout << "--------------\n";
  // std::cout << "min=" << m << " - max=" << M << " - moy=" << moy << " - nb_diff=" << nb_diff << " / " << i << std::endl;

  return 0;
}
