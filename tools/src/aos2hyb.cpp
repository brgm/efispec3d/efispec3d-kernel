#include <iostream>
#include <fstream>
#include <vector>

#include <boost/align/aligned_allocator.hpp>


void aos2hyb( std::string const & filename0, std::string const & filename1 )
{
  uint32_t dims = 0;

  std::vector< float > vin;
  std::vector< float > vout;
  std::vector< std::size_t > vdims;

  std::ifstream ifs( filename0, std::ios_base::in
                              | std::ios_base::binary );
  std::ofstream ofs( filename1, std::ios_base::out
                              | std::ios_base::binary );

  ifs.read( reinterpret_cast<char*>( &dims), sizeof( uint32_t ) );

  ofs.write( reinterpret_cast<char*>( &dims), sizeof( uint32_t ) );

  vdims.resize( dims );

  std::size_t size = 1;
  for( uint32_t i = 0 ; i < dims ; ++i )
  {
    uint32_t dim;
    ifs.read( reinterpret_cast<char*>( & dim ), sizeof( uint32_t ) );
    vdims[ i ] = dim;
    size *= dim;
  }
  for( uint32_t i = 0 ; i < dims ; ++i )
  {
    std::cout << vdims[ i ] << std::endl;
    ofs.write( reinterpret_cast<char*>( &vdims[ (i + dims ) % dims ] ), sizeof( uint32_t ) );
  }

  vin.resize( size );
  vout.resize( size );

  ifs.read( reinterpret_cast<char*>( vin.data() ), size * sizeof( float ) );

/*
  for( std::size_t k = 0 ; k < vdims[ 0 ] ; ++k )
  {
    for( std::size_t l = 0 ; l < vdims[ 1 ] ; ++l )
    {
      for( std::size_t m = 0 ; m < vdims[ 2 ] ; ++m )
      {
        for( std::size_t iel = 0 ; iel < vdims[ 3 ] ; ++iel )
        {
          vout[ vdims[3] *( m + l * 5 + k * 25 ) + iel ] = vin[ m + l * 5 + k * 25 + iel * 125 ];
        }
      }
    }
  }
*/
  for( std::size_t iel = 0 ; iel < vdims[ 3 ] ; iel += 8 )
  {
    for( std::size_t k = 0 ; k < vdims[ 0 ] ; ++k )
    {
      for( std::size_t l = 0 ; l < vdims[ 1 ] ; ++l )
      {
        for( std::size_t m = 0 ; m < vdims[ 2 ] ; ++m )
        {
          for( std::size_t i = 0 ; i < 8 ; ++i )
          {
            vout[ iel * 125 + 8 * ( m + l * 5 + k * 25 ) + i ] = vin[ m + l * 5 + k * 25 + ( iel + i ) * 125 ];
          }
        }
      }
    }
  }

  ofs.write( reinterpret_cast<char*>( vout.data() ), vout.size() * sizeof( float ) );

}



int main()
{
  //std::string path = "../data/";
  std::string path = "../data/Elast40000/";

  aos2hyb( path + "rg_hexa_gll_dxidx.dat", path + "rg_hexa_gll_dxidx-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_dxidy.dat", path + "rg_hexa_gll_dxidy-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_dxidz.dat", path + "rg_hexa_gll_dxidz-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_detdx.dat", path + "rg_hexa_gll_detdx-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_detdy.dat", path + "rg_hexa_gll_detdy-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_detdz.dat", path + "rg_hexa_gll_detdz-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_dzedx.dat", path + "rg_hexa_gll_dzedx-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_dzedy.dat", path + "rg_hexa_gll_dzedy-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_dzedz.dat", path + "rg_hexa_gll_dzedz-hyb.dat");

  aos2hyb( path + "rg_hexa_gll_rhovp2.dat", path + "rg_hexa_gll_rhovp2-hyb.dat");
  aos2hyb( path + "rg_hexa_gll_rhovs2.dat", path + "rg_hexa_gll_rhovs2-hyb.dat");

  aos2hyb( path + "rg_hexa_gll_jacobian_det.dat", path + "rg_hexa_gll_jacobian_det-hyb.dat");

  return 0;
}
