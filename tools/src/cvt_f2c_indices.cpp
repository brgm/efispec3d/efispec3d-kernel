#include <fstream>
#include <vector>


int main()
{
  std::ifstream ifs( "../data/Elast40000/ig_hexa_gll_glonum.dat");

  std::ofstream ofs( "../data/Elast40000/ig_hexa_gll_glonum-C.dat");

  std::vector< uint32_t > v;

  std::uint32_t nbdims;

  std::vector< uint32_t > dims;
  
  ifs.read( (char*)&nbdims, sizeof( std::uint32_t ) );
  ofs.write( (char*)&nbdims, sizeof( std::uint32_t ) );
  
  dims.resize( nbdims );
  
  std::size_t size = 1;
  for( std::uint32_t i = 0 ; i < dims.size() ; ++i )
  {
    ifs.read( (char *)&dims[ i ], sizeof( std::uint32_t ) );
    ofs.write( (char *)&dims[ i ], sizeof( std::uint32_t ) );
    size *= dims[ i ];
  }

  v.resize( size );

  ifs.read( (char *)v.data(), size * sizeof( std::uint32_t ) );

  for( auto & i: v )
  {
    i -= 1;
  }

  ofs.write( (char *)v.data(), size * sizeof( std::uint32_t ) );

  ifs.close();

  ofs.close();
  
  return 0;
}
