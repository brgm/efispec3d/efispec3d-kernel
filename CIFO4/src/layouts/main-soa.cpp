#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>

#include <cifo4.hpp>



#define IDX2( m, l ) ( 5 * l + m )
#define IDX3( m, l, k ) ( 25 * k + 5 * l + m )
#define IDX4( m, l, k, iel ) ( 125 * (iel) + 25 * (k) + 5 * (l) + (m) )


/**
 * Fill a std::vector from a file.
 */
template < typename T >
void vfill( std::string const filename, std::vector< T, boost::alignment::aligned_allocator< T, 64 >  > & v )
{
  uint32_t dims = 0;
  std::ifstream ifs( filename , std::ios_base::in
                              | std::ios_base::binary );

  ifs.read( reinterpret_cast<char*>( &dims ), sizeof( uint32_t ) );

  std::size_t size = 1;
  for( uint32_t i = 0 ; i < dims ; ++i )
  {
    uint32_t dim;
    ifs.read( reinterpret_cast<char*>( & dim ), sizeof( uint32_t ) );
    size *= dim;
  }

  v.resize( size );

  ifs.read( reinterpret_cast<char*>( v.data() ), size * sizeof( T ) );
}


/**
 * Save a vector to a file.
 */
template < typename T >
void save( std::string const filename, std::vector< T, boost::alignment::aligned_allocator< T, 64 >  > & v )
{
  uint32_t dims = 2;
  uint32_t dim = 3;
  uint32_t size = v.size();

  std::ofstream ofs( filename, std::ios_base::out
                            | std::ios_base::binary );
  ofs.write( reinterpret_cast<char*>( &dims ), sizeof( dims ) );
  ofs.write( reinterpret_cast<char*>( &dim ), sizeof( dim ) );
  ofs.write( reinterpret_cast<char*>( &size ), sizeof( size ) );
  ofs.write( reinterpret_cast<char*>( v.data() ), v.size() * sizeof( T ) );
}


int main( int argc, char * argv[] )
{
    //std::string path = "../data/";
  std::string path = "../data/Elast40000/";

  vfill( path + "ig_hexa_gll_glonum-C-soa.dat", ig_hexa_gll_glonum );

  vfill( path + "rg_gll_displacement.dat", rg_gll_displacement );
  vfill( path + "rg_gll_lagrange_deriv.dat", rg_gll_lagrange_deriv );

  vfill( path + "rg_hexa_gll_dxidx-soa.dat", rg_hexa_gll_dxidx );
  vfill( path + "rg_hexa_gll_dxidy-soa.dat", rg_hexa_gll_dxidy );
  vfill( path + "rg_hexa_gll_dxidz-soa.dat", rg_hexa_gll_dxidz );

  vfill( path + "rg_hexa_gll_detdx-soa.dat", rg_hexa_gll_detdx );
  vfill( path + "rg_hexa_gll_detdy-soa.dat", rg_hexa_gll_detdy );
  vfill( path + "rg_hexa_gll_detdz-soa.dat", rg_hexa_gll_detdz );

  vfill( path + "rg_hexa_gll_dzedx-soa.dat", rg_hexa_gll_dzedx );
  vfill( path + "rg_hexa_gll_dzedy-soa.dat", rg_hexa_gll_dzedy );
  vfill( path + "rg_hexa_gll_dzedz-soa.dat", rg_hexa_gll_dzedz );

  vfill( path + "rg_hexa_gll_rhovp2-soa.dat", rg_hexa_gll_rhovp2 );
  vfill( path + "rg_hexa_gll_rhovs2-soa.dat", rg_hexa_gll_rhovs2 );
  vfill( path + "rg_hexa_gll_jacobian_det-soa.dat", rg_hexa_gll_jacobian_det );
  vfill( path + "rg_gll_weight.dat", rg_gll_weight );
  vfill( path + "rg_gll_acceleration_before_loop_iel.dat", rg_gll_acceleration );

  std::vector< float, boost::alignment::aligned_allocator< float, 64 > > out_ref;
  vfill( path + "rg_gll_acceleration_after_loop_iel.dat", out_ref );

  auto iel = ig_hexa_gll_glonum.size()/125;

  std::cout << "iel = " << iel << std::endl;
  /*
  rg_gll_displacement_soa.resize( 3*125*iel );

  for( std::size_t i = 0 ; i < iel ; i+=8 )
  {
    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          for( std::size_t j = 0 ; j < 8 ; ++j)
          {
            rg_gll_displacement_soa[  0 + i * 125 + 24 * ( m + l * 5 + k * 25 ) + j ] = rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, i + j ) ] ) ];
            rg_gll_displacement_soa[  8 + i * 125 + 24 * ( m + l * 5 + k * 25 ) + j ] = rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, i + j ) ] ) ];
            rg_gll_displacement_soa[ 16 + i * 125 + 24 * ( m + l * 5 + k * 25 ) + j ] = rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, i + j ) ] ) ];
          }
        }
      }
    }
  }
*/

  auto start = std::chrono::system_clock::now();

  compute_internal_forces_order4( 0, iel );

  auto stop = std::chrono::system_clock::now();

  auto duration = stop - start;

  std::cout << std::chrono::duration_cast< std::chrono::milliseconds >( duration ).count() << std::endl;
/*
  // Display results.
  for( std::size_t i = 0 ; i < out_ref.size() ; ++i )
  {
    std::cout << out_ref[ i ] << ' ' << rg_gll_acceleration[ i ] << std::endl;
  }
*/
  save( std::string(argv[0])+".dat", rg_gll_acceleration );

  return 0;
}
