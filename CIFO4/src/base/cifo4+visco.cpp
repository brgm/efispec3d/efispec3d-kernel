#include <cstddef>
#include <iostream>

#include <cifo4.hpp>


#define IDX2( m, l ) ( 5 * l + m )
#define IDX3( m, l, k ) ( 25 * k + 5 * l + m )
#define IDX4( m, l, k, iel ) ( 125 * iel + 25 * k + 5 * l + m )


veci ig_hexa_gll_glonum;

vecf rg_gll_displacement;
vecf rg_gll_weight;

vecf rg_gll_lagrange_deriv;
vecf rg_gll_acceleration;

vecf rg_hexa_gll_dxidx;
vecf rg_hexa_gll_dxidy;
vecf rg_hexa_gll_dxidz;
vecf rg_hexa_gll_detdx;
vecf rg_hexa_gll_detdy;
vecf rg_hexa_gll_detdz;
vecf rg_hexa_gll_dzedx;
vecf rg_hexa_gll_dzedy;
vecf rg_hexa_gll_dzedz;

vecf rg_hexa_gll_rhovp2;
vecf rg_hexa_gll_rhovs2;
vecf rg_hexa_gll_jacobian_det;

//VISCO
vecf rg_mem_var_exp;
vecf rg_hexa_gll_wkqp;
vecf rg_hexa_gll_wkqs;
vecf rg_hexa_gll_ksixx;
vecf rg_hexa_gll_ksiyy;
vecf rg_hexa_gll_ksizz;
vecf rg_hexa_gll_ksixy;
vecf rg_hexa_gll_ksixz;
vecf rg_hexa_gll_ksiyz;
//

void compute_internal_forces_order4( std::size_t elt_start, std::size_t elt_end )
{
  float rl_displacement_gll[5*5*5*3];

  // Allocate all the local arrays at once ( 5 * 5 * 5 * 9 * 4 = 4.5 kB ).
  float local[ 5 * 5 * 5 * 9 ];

  float * intpx1 = &local[    0 ];
  float * intpy1 = &local[  125 ];
  float * intpz1 = &local[  250 ];

  float * intpx2 = &local[  375 ];
  float * intpy2 = &local[  500 ];
  float * intpz2 = &local[  625 ];

  float * intpx3 = &local[  750 ];
  float * intpy3 = &local[  875 ];
  float * intpz3 = &local[ 1000 ];


  for( std::size_t iel = elt_start ; iel < elt_end ; ++iel )
  {
    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          auto igll = ig_hexa_gll_glonum[ IDX4( 0, l, k, iel ) ] - 1; // Fortran!
          auto coeff = rg_gll_lagrange_deriv[ IDX2( 0, m ) ];

          auto duxdxi = rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          auto duydxi = rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          auto duzdxi = rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( 1, l, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 1, m ) ];

          duxdxi += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydxi += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdxi += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( 2, l, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 2, m ) ];

          duxdxi += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydxi += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdxi += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( 3, l, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 3, m ) ];

          duxdxi += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydxi += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdxi += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( 4, l, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 4, m ) ];

          duxdxi += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydxi += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdxi += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          //

          igll = ig_hexa_gll_glonum[ IDX4( m, 0, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 0, l ) ];

          auto duxdet = rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          auto duydet = rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          auto duzdet = rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, 1, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 1, l ) ];

          duxdet += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydet += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdet += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, 2, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 2, l ) ];

          duxdet += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydet += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdet += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, 3, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 3, l ) ];

          duxdet += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydet += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdet += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, 4, k, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 4, l ) ];

          duxdet += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydet += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdet += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          //

          igll = ig_hexa_gll_glonum[ IDX4( m, l, 0, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 0, k ) ];

          auto duxdze = rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          auto duydze = rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          auto duzdze = rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, l, 1, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 1, k ) ];

          duxdze += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydze += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdze += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, l, 2, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 2, k ) ];

          duxdze += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydze += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdze += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, l, 3, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 3, k ) ];

          duxdze += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydze += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdze += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          igll = ig_hexa_gll_glonum[ IDX4( m, l, 4, iel ) ] - 1;
          coeff = rg_gll_lagrange_deriv[ IDX2( 4, k ) ];

          duxdze += rg_gll_displacement[ 0 + 3 * igll ] * coeff;
          duydze += rg_gll_displacement[ 1 + 3 * igll ] * coeff;
          duzdze += rg_gll_displacement[ 2 + 3 * igll ] * coeff;

          //

          auto dxidx = rg_hexa_gll_dxidx[ IDX4( m, l, k, iel ) ];
          auto detdx = rg_hexa_gll_detdx[ IDX4( m, l, k, iel ) ];
          auto dzedx = rg_hexa_gll_dzedx[ IDX4( m, l, k, iel ) ];

          auto duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx;
          auto duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx;
          auto duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx;

          auto dxidy = rg_hexa_gll_dxidy[ IDX4( m, l, k, iel ) ];
          auto detdy = rg_hexa_gll_detdy[ IDX4( m, l, k, iel ) ];
          auto dzedy = rg_hexa_gll_dzedy[ IDX4( m, l, k, iel ) ];

          auto duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy;
          auto duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy;
          auto duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy;

          auto dxidz = rg_hexa_gll_dxidz[ IDX4( m, l, k, iel ) ];
          auto detdz = rg_hexa_gll_detdz[ IDX4( m, l, k, iel ) ];
          auto dzedz = rg_hexa_gll_dzedz[ IDX4( m, l, k, iel ) ];

          auto duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz;
          auto duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz;
          auto duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz;

          // Load 8 values from rg_hexa_gll_rhovp2 and rg_hexa_gll_rhovs2.
          auto rhovp2 = rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel ) ];
          auto rhovs2 = rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel ) ];

          auto trace_tau = ( rhovp2 - 2.0 * rhovs2 )*(duxdx+duydy+duzdz);
          auto tauxx     = trace_tau + 2.0*rhovs2*duxdx;
          auto tauyy     = trace_tau + 2.0*rhovs2*duydy;
          auto tauzz     = trace_tau + 2.0*rhovs2*duzdz;
          auto tauxy     =                 rhovs2*(duxdy+duydx);
          auto tauxz     =                 rhovs2*(duxdz+duzdx);
          auto tauyz     =                 rhovs2*(duydz+duzdy);

// VISCO
          for( std::size_t imem_var = 0 ; imem_var < 8 ; ++imem_var )
          {
            auto tmpx1 = rg_mem_var_exp[ imem_var ];
            auto tmpx2 = rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel ) ]*rg_hexa_gll_wkqp[ imem_var + 8 * IDX4( m, l, k, iel ) ];
            auto tmpx3 = 2.0*rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel ) ]*rg_hexa_gll_wkqs[ imem_var + 8 * IDX4( m, l, k, iel ) ];
            auto tmpx4 = ( duxdx + duydy + duzdz )*( tmpx2 - tmpx3 );

            auto tauxx_n12 = tmpx1 * rg_hexa_gll_ksixx[ imem_var + 8 * IDX4( m, l, k, iel ) ] + (1.0 - tmpx1) * (tmpx3*duxdx + tmpx4);
            auto tauyy_n12 = tmpx1 * rg_hexa_gll_ksiyy[ imem_var + 8 * IDX4( m, l, k, iel ) ] + (1.0 - tmpx1) * (tmpx3*duydy + tmpx4);
            auto tauzz_n12 = tmpx1 * rg_hexa_gll_ksizz[ imem_var + 8 * IDX4( m, l, k, iel ) ] + (1.0 - tmpx1) * (tmpx3*duzdz + tmpx4);
            auto tauxy_n12 = tmpx1 * rg_hexa_gll_ksixy[ imem_var + 8 * IDX4( m, l, k, iel ) ] + (1.0 - tmpx1) * (tmpx3*0.5*(duxdy+duydx));
            auto tauxz_n12 = tmpx1 * rg_hexa_gll_ksixz[ imem_var + 8 * IDX4( m, l, k, iel ) ] + (1.0 - tmpx1) * (tmpx3*0.5*(duxdz+duzdx));
            auto tauyz_n12 = tmpx1 * rg_hexa_gll_ksiyz[ imem_var + 8 * IDX4( m, l, k, iel ) ] + (1.0 - tmpx1) * (tmpx3*0.5*(duydz+duzdy));

            tauxx = tauxx - 0.5 * ( tauxx_n12 + rg_hexa_gll_ksixx[ imem_var + 8 * IDX4( m, l, k, iel ) ] );
            tauyy = tauyy - 0.5 * ( tauyy_n12 + rg_hexa_gll_ksiyy[ imem_var + 8 * IDX4( m, l, k, iel ) ] );
            tauzz = tauzz - 0.5 * ( tauzz_n12 + rg_hexa_gll_ksizz[ imem_var + 8 * IDX4( m, l, k, iel ) ] );
            tauxy = tauxy - 0.5 * ( tauxy_n12 + rg_hexa_gll_ksixy[ imem_var + 8 * IDX4( m, l, k, iel ) ] );
            tauxz = tauxz - 0.5 * ( tauxz_n12 + rg_hexa_gll_ksixz[ imem_var + 8 * IDX4( m, l, k, iel ) ] );
            tauyz = tauyz - 0.5 * ( tauyz_n12 + rg_hexa_gll_ksiyz[ imem_var + 8 * IDX4( m, l, k, iel ) ] );

            rg_hexa_gll_ksixx[ imem_var + 8 * IDX4( m, l, k, iel ) ] = tauxx_n12;
            rg_hexa_gll_ksiyy[ imem_var + 8 * IDX4( m, l, k, iel ) ] = tauyy_n12;
            rg_hexa_gll_ksizz[ imem_var + 8 * IDX4( m, l, k, iel ) ] = tauzz_n12;
            rg_hexa_gll_ksixy[ imem_var + 8 * IDX4( m, l, k, iel ) ] = tauxy_n12;
            rg_hexa_gll_ksixz[ imem_var + 8 * IDX4( m, l, k, iel ) ] = tauxz_n12;
            rg_hexa_gll_ksiyz[ imem_var + 8 * IDX4( m, l, k, iel ) ] = tauyz_n12;
          }
//

          intpx1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
          intpx2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*detdx+tauxy*detdy+tauxz*detdz);
          intpx3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

          intpy1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
          intpy2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*detdx+tauyy*detdy+tauyz*detdz);
          intpy3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

          intpz1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
          intpz2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*detdx+tauyz*detdy+tauzz*detdz);
          intpz3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
        }
      }
    }

    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          auto tmpx1 = intpx1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpx1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpx1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpx1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpx1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpy1 = intpy1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpy1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpy1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpy1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpy1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpz1 = intpz1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpz1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpz1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpz1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpz1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv[ IDX2( m, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpx2 = intpx2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpx2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpx2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpx2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpx2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpy2 = intpy2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpy2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpy2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpy2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpy2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpz2 = intpz2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpz2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpz2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpz2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpz2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv[ IDX2( l, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpx3 = intpx3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpx3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpx3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpx3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpx3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpy3 = intpy3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpy3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpy3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpy3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpy3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 4 ) ]*rg_gll_weight[ 4 ];

          auto tmpz3 = intpz3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 0 ) ]*rg_gll_weight[ 0 ]
                     + intpz3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 1 ) ]*rg_gll_weight[ 1 ]
                     + intpz3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 2 ) ]*rg_gll_weight[ 2 ]
                     + intpz3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 3 ) ]*rg_gll_weight[ 3 ]
                     + intpz3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv[ IDX2( k, 4 ) ]*rg_gll_weight[ 4 ];

          auto fac1 = rg_gll_weight[ l ]*rg_gll_weight[ k ];
          auto fac2 = rg_gll_weight[ m ]*rg_gll_weight[ k ];
          auto fac3 = rg_gll_weight[ m ]*rg_gll_weight[ l ];

          auto index = ig_hexa_gll_glonum[ IDX4( m, l, k, iel ) ] - 1;

          rg_gll_acceleration[ 0 + 3 * index ] -= fac1 * tmpx1 + fac2 * tmpx2 + fac3 * tmpx3;
          rg_gll_acceleration[ 1 + 3 * index ] -= fac1 * tmpy1 + fac2 * tmpy2 + fac3 * tmpy3;
          rg_gll_acceleration[ 2 + 3 * index ] -= fac1 * tmpz1 + fac2 * tmpz2 + fac3 * tmpz3;
        }
      }
    }
  }
}
