#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>
#include <cmath>

#include <cifo4-cuda.hpp>

// #include <papi.h>

/**
 * Fill a std::vector from a file.
 */
template < typename T >
void vfill( std::string const filename, std::vector< T, boost::alignment::aligned_allocator< T, 64 >  > & v )
{
  uint32_t dims = 0;
  std::ifstream ifs( filename , std::ios_base::in
                              | std::ios_base::binary );

  ifs.read( reinterpret_cast<char*>( &dims ), sizeof( uint32_t ) );
  
  std::size_t size = 1;
  for( uint32_t i = 0 ; i < dims ; ++i )
  {
    uint32_t dim;
    ifs.read( reinterpret_cast<char*>( & dim ), sizeof( uint32_t ) );
    size *= dim;
  }

  v.resize( size );

  // std::cout << filename;
  // printf(" size : %d\n", size);

  ifs.read( reinterpret_cast<char*>( v.data() ), size * sizeof( T ) );
}


/**
 * Save a vector to a file.
 */
template < typename T >
void save( std::string const filename, std::vector< T, boost::alignment::aligned_allocator< T, 64 >  > & v )
{
  uint32_t dims = 2;
  uint32_t dim = 3;
  uint32_t size = v.size() / 3;

  std::ofstream ofs( filename, std::ios_base::out
                            | std::ios_base::binary );
  //std::cout << dims << ' ' << dim << ' ' << size << std::endl;
  ofs.write( reinterpret_cast<char*>( &dims ), sizeof( dims ) );
  ofs.write( reinterpret_cast<char*>( &dim ), sizeof( dim ) );
  ofs.write( reinterpret_cast<char*>( &size ), sizeof( size ) );
  ofs.write( reinterpret_cast<char*>( v.data() ), v.size() * sizeof( T ) );
}


int main( int argc, char * argv[] )
{

  veci ig_hexa_gll_glonum;

  vecf rg_gll_displacement;
  vecf rg_gll_weight;

  vecf rg_gll_lagrange_deriv;
  vecf rg_gll_acceleration;

  vecf rg_hexa_gll_dxidx;
  vecf rg_hexa_gll_dxidy;
  vecf rg_hexa_gll_dxidz;
  vecf rg_hexa_gll_detdx;
  vecf rg_hexa_gll_detdy;
  vecf rg_hexa_gll_detdz;
  vecf rg_hexa_gll_dzedx;
  vecf rg_hexa_gll_dzedy;
  vecf rg_hexa_gll_dzedz;

  vecf rg_hexa_gll_rhovp2;
  vecf rg_hexa_gll_rhovs2;
  vecf rg_hexa_gll_jacobian_det;

  cudaEvent_t start, stop;
  float duration = 0.0;

  uint32_t * d_ig_hexa_gll_glonum;
  float * d_rg_gll_displacement;
  float * d_rg_gll_lagrange_deriv;
  float * d_rg_hexa_gll_dxidx;
  float * d_rg_hexa_gll_dxidy;
  float * d_rg_hexa_gll_dxidz;
  float * d_rg_hexa_gll_detdx;
  float * d_rg_hexa_gll_detdy;
  float * d_rg_hexa_gll_detdz;
  float * d_rg_hexa_gll_dzedx;
  float * d_rg_hexa_gll_dzedy;
  float * d_rg_hexa_gll_dzedz;
  float * d_rg_hexa_gll_rhovp2;
  float * d_rg_hexa_gll_rhovs2;
  float * d_rg_hexa_gll_jacobian_det;
  float * d_rg_gll_weight;
  float * d_rg_gll_acceleration;

  cudaEventCreate(&start);
  cudaEventCreate(&stop);


  //std::string path = "../data/Elast4176/";
  std::string path = "../data/Elast40000/";

  vfill( path + "ig_hexa_gll_glonum-C.dat",                ig_hexa_gll_glonum );

  vfill( path + "rg_gll_displacement.dat",                 rg_gll_displacement );
  vfill( path + "rg_gll_lagrange_deriv.dat",               rg_gll_lagrange_deriv );

  vfill( path + "rg_hexa_gll_dxidx.dat",                   rg_hexa_gll_dxidx );
  vfill( path + "rg_hexa_gll_dxidy.dat",                   rg_hexa_gll_dxidy );
  vfill( path + "rg_hexa_gll_dxidz.dat",                   rg_hexa_gll_dxidz );

  vfill( path + "rg_hexa_gll_detdx.dat",                   rg_hexa_gll_detdx );
  vfill( path + "rg_hexa_gll_detdy.dat",                   rg_hexa_gll_detdy );
  vfill( path + "rg_hexa_gll_detdz.dat",                   rg_hexa_gll_detdz );

  vfill( path + "rg_hexa_gll_dzedx.dat",                   rg_hexa_gll_dzedx );
  vfill( path + "rg_hexa_gll_dzedy.dat",                   rg_hexa_gll_dzedy );
  vfill( path + "rg_hexa_gll_dzedz.dat",                   rg_hexa_gll_dzedz );

  vfill( path + "rg_hexa_gll_rhovp2.dat",                  rg_hexa_gll_rhovp2 );
  vfill( path + "rg_hexa_gll_rhovs2.dat",                  rg_hexa_gll_rhovs2 );
  vfill( path + "rg_hexa_gll_jacobian_det.dat",            rg_hexa_gll_jacobian_det );
  vfill( path + "rg_gll_weight.dat",                       rg_gll_weight );
  vfill( path + "rg_gll_acceleration_before_loop_iel.dat", rg_gll_acceleration );


  cudaMalloc( (void**)&d_ig_hexa_gll_glonum,       ig_hexa_gll_glonum.size() * sizeof( uint32_t )    );

  cudaMalloc( (void**)&d_rg_gll_displacement,      rg_gll_displacement.size() * sizeof( float )      );
  cudaMalloc( (void**)&d_rg_gll_lagrange_deriv,    rg_gll_lagrange_deriv.size() * sizeof( float )    );

  cudaMalloc( (void**)&d_rg_hexa_gll_dxidx,        rg_hexa_gll_dxidx.size() * sizeof( float )        );
  cudaMalloc( (void**)&d_rg_hexa_gll_dxidy,        rg_hexa_gll_dxidy.size() * sizeof( float )        );
  cudaMalloc( (void**)&d_rg_hexa_gll_dxidz,        rg_hexa_gll_dxidz.size() * sizeof( float )        );

  cudaMalloc( (void**)&d_rg_hexa_gll_detdx,        rg_hexa_gll_detdx.size() * sizeof( float )        );
  cudaMalloc( (void**)&d_rg_hexa_gll_detdy,        rg_hexa_gll_detdy.size() * sizeof( float )        );
  cudaMalloc( (void**)&d_rg_hexa_gll_detdz,        rg_hexa_gll_detdz.size() * sizeof( float )        );

  cudaMalloc( (void**)&d_rg_hexa_gll_dzedx,        rg_hexa_gll_dzedx.size() * sizeof( float )        );
  cudaMalloc( (void**)&d_rg_hexa_gll_dzedy,        rg_hexa_gll_dzedy.size() * sizeof( float )        );
  cudaMalloc( (void**)&d_rg_hexa_gll_dzedz,        rg_hexa_gll_dzedz.size() * sizeof( float )        );

  cudaMalloc( (void**)&d_rg_hexa_gll_rhovp2,       rg_hexa_gll_rhovp2.size() * sizeof( float )       );
  cudaMalloc( (void**)&d_rg_hexa_gll_rhovs2,       rg_hexa_gll_rhovs2.size() * sizeof( float )       );
  cudaMalloc( (void**)&d_rg_hexa_gll_jacobian_det, rg_hexa_gll_jacobian_det.size() * sizeof( float ) );
  cudaMalloc( (void**)&d_rg_gll_weight,            rg_gll_weight.size() * sizeof( float )            );
  cudaMalloc( (void**)&d_rg_gll_acceleration,      rg_gll_acceleration.size() * sizeof( float )      );


  cudaMemcpy( d_ig_hexa_gll_glonum,       ig_hexa_gll_glonum.data(),       ig_hexa_gll_glonum.size() * sizeof( uint32_t ),    cudaMemcpyHostToDevice );

  cudaMemcpy( d_rg_gll_displacement,      rg_gll_displacement.data(),      rg_gll_displacement.size() * sizeof( float ),      cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_gll_lagrange_deriv,    rg_gll_lagrange_deriv.data(),    rg_gll_lagrange_deriv.size() * sizeof( float ),    cudaMemcpyHostToDevice );

  cudaMemcpy( d_rg_hexa_gll_dxidx,        rg_hexa_gll_dxidx.data(),        rg_hexa_gll_dxidx.size() * sizeof( float ),        cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_dxidy,        rg_hexa_gll_dxidy.data(),        rg_hexa_gll_dxidy.size() * sizeof( float ),        cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_dxidz,        rg_hexa_gll_dxidz.data(),        rg_hexa_gll_dxidz.size() * sizeof( float ),        cudaMemcpyHostToDevice );

  cudaMemcpy( d_rg_hexa_gll_detdx,        rg_hexa_gll_detdx.data(),        rg_hexa_gll_detdx.size() * sizeof( float ),        cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_detdy,        rg_hexa_gll_detdy.data(),        rg_hexa_gll_detdy.size() * sizeof( float ),        cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_detdz,        rg_hexa_gll_detdz.data(),        rg_hexa_gll_detdz.size() * sizeof( float ),        cudaMemcpyHostToDevice );

  cudaMemcpy( d_rg_hexa_gll_dzedx,        rg_hexa_gll_dzedx.data(),        rg_hexa_gll_dzedx.size() * sizeof( float ),        cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_dzedy,        rg_hexa_gll_dzedy.data(),        rg_hexa_gll_dzedy.size() * sizeof( float ),        cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_dzedz,        rg_hexa_gll_dzedz.data(),        rg_hexa_gll_dzedz.size() * sizeof( float ),        cudaMemcpyHostToDevice );
  
  cudaMemcpy( d_rg_hexa_gll_rhovp2,       rg_hexa_gll_rhovp2.data(),       rg_hexa_gll_rhovp2.size() * sizeof( float ),       cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_rhovs2,       rg_hexa_gll_rhovs2.data(),       rg_hexa_gll_rhovs2.size() * sizeof( float ),       cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_hexa_gll_jacobian_det, rg_hexa_gll_jacobian_det.data(), rg_hexa_gll_jacobian_det.size() * sizeof( float ), cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_gll_weight,            rg_gll_weight.data(),            rg_gll_weight.size() * sizeof( float ),            cudaMemcpyHostToDevice );
  cudaMemcpy( d_rg_gll_acceleration,      rg_gll_acceleration.data(),      rg_gll_acceleration.size() * sizeof( float ),      cudaMemcpyHostToDevice );

  std::size_t nbthreads = 32; 

  dim3 dimGrid(1, 1, 1);
  dim3 dimBlock(nbthreads, 1, 1);

  // auto start = std::chrono::system_clock::now();

  cudaEventRecord(start);

  compute_internal_forces_order4<<<dimGrid, dimBlock>>>( 0, ig_hexa_gll_glonum.size()/125, 

                                                        d_ig_hexa_gll_glonum, d_rg_gll_displacement, d_rg_gll_lagrange_deriv,

                                                        d_rg_hexa_gll_dxidx, d_rg_hexa_gll_dxidy, d_rg_hexa_gll_dxidz, 
                                                        d_rg_hexa_gll_detdx, d_rg_hexa_gll_detdy, d_rg_hexa_gll_detdz, 
                                                        d_rg_hexa_gll_dzedx, d_rg_hexa_gll_dzedy, d_rg_hexa_gll_dzedz,

                                                        d_rg_hexa_gll_rhovp2, d_rg_hexa_gll_rhovs2,
                                                        d_rg_hexa_gll_jacobian_det, d_rg_gll_weight,

                                                        d_rg_gll_acceleration );

  // auto stop = std::chrono::system_clock::now();
  // auto duration = stop - start;
  // std::cout << std::chrono::duration_cast< std::chrono::milliseconds >( duration ).count() << std::endl;

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);

  cudaEventElapsedTime(&duration, start, stop);

  std::cout << duration << " ms" << std::endl;

  cudaMemcpy( rg_gll_acceleration.data(), d_rg_gll_acceleration, rg_gll_acceleration.size() * sizeof( float ), cudaMemcpyDeviceToHost );

  cudaError_t err = cudaGetLastError();

  printf("Error cuda: %s\n", cudaGetErrorString(err));


  save( std::string(argv[0])+".dat", rg_gll_acceleration );

  cudaFree( d_ig_hexa_gll_glonum       );
  cudaFree( d_rg_gll_displacement      );
  cudaFree( d_rg_gll_lagrange_deriv    );
  cudaFree( d_rg_hexa_gll_dxidx        );
  cudaFree( d_rg_hexa_gll_dxidy        );
  cudaFree( d_rg_hexa_gll_dxidz        );
  cudaFree( d_rg_hexa_gll_detdx        );
  cudaFree( d_rg_hexa_gll_detdy        );
  cudaFree( d_rg_hexa_gll_detdz        );
  cudaFree( d_rg_hexa_gll_dzedx        );
  cudaFree( d_rg_hexa_gll_dzedy        );
  cudaFree( d_rg_hexa_gll_dzedz        );
  cudaFree( d_rg_hexa_gll_rhovp2       );
  cudaFree( d_rg_hexa_gll_rhovs2       );
  cudaFree( d_rg_hexa_gll_jacobian_det );
  cudaFree( d_rg_gll_weight            );
  cudaFree( d_rg_gll_acceleration      );  

  return 0;
}

