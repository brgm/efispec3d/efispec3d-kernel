#include <cstddef>
#include <iostream>

#include <cifo4-cuda.hpp>


#define IDX2( m, l ) ( 5 * l + m )
#define IDX3( m, l, k ) ( 25 * k + 5 * l + m )
#define IDX3X3( x, m, l, k ) ( 3 * ( 25 * k + 5 * l + m ) + x )
#define IDX3X3PAD( x, m, l, k ) ( 3 * ( 25 * k + 5 * l + m ) + x + (3 * ( 25 * k + 5 * l + m ) + x)/32 )
#define IDX4( m, l, k, iel ) ( 125 * iel + 25 * k + 5 * l + m )


__global__ void compute_internal_forces_order4( std::size_t elt_start, std::size_t elt_end, uint32_t * ig_hexa_gll_glonum, float * rg_gll_displacement, float * rg_gll_lagrange_deriv, float * rg_hexa_gll_dxidx, float * rg_hexa_gll_dxidy, float * rg_hexa_gll_dxidz, float * rg_hexa_gll_detdx, float * rg_hexa_gll_detdy, float * rg_hexa_gll_detdz, float * rg_hexa_gll_dzedx, float * rg_hexa_gll_dzedy, float * rg_hexa_gll_dzedz, float * rg_hexa_gll_rhovp2, float * rg_hexa_gll_rhovs2, float * rg_hexa_gll_jacobian_det, float * rg_gll_weight, float * rg_gll_acceleration)
{

  std::size_t idx = threadIdx.x;

  std::size_t nbthreads = 32;

  // std::size_t nb_elem = elt_end - elt_start;


  // float rl_displacement_gll[5*5*5*3];

  extern __shared__ float shared[];

  float * rl_displacement_gll = &shared[idx * 5*5*5*3 + (idx * 5*5*5*3)/32];

  // float * rg_hexa_gll_rhovp2_shared = &shared[5*5*5*3 * (32+1)];

  // float * rg_hexa_gll_rhovs2_shared = &shared[5*5*5*3 * (32+1) + 125 * nb_elem];

  // float * rg_hexa_gll_jacobian_det_shared = &shared[5*5*5*3 * (32+1) + 125 * nb_elem * 2];

  // if(idx == 0) {
  //   for( std::size_t i = 0 ; i <= nb_elem * 125 ; ++i ) {
  //     rg_hexa_gll_rhovp2_shared[i] = rg_hexa_gll_rhovp2[i];
  //     rg_hexa_gll_rhovs2_shared[i] = rg_hexa_gll_rhovs2[i];
  //     rg_hexa_gll_jacobian_det_shared[i] = rg_hexa_gll_jacobian_det[i];
  //   }
  // }

  __shared__ float rg_gll_lagrange_deriv_shared[5*5];

  __shared__ float rg_gll_weight_shared[5];

  if (idx < 25) {
	  rg_gll_lagrange_deriv_shared[idx] = rg_gll_lagrange_deriv[idx];
  }

  if (idx < 5) {
	  rg_gll_weight_shared[idx] = rg_gll_weight[idx];
  }

  // __syncthreads();


  // Allocate all the local arrays at once ( 5 * 5 * 5 * 9 * 4 = 4.5 kB ).
  float local[ 5 * 5 * 5 * 9 ];


  float * intpx1 = &local[    0 ];
  float * intpy1 = &local[  125 ];
  float * intpz1 = &local[  250 ];

  float * intpx2 = &local[  375 ];
  float * intpy2 = &local[  500 ];
  float * intpz2 = &local[  625 ];

  float * intpx3 = &local[  750 ];
  float * intpy3 = &local[  875 ];
  float * intpz3 = &local[ 1000 ];

  std::size_t elt_32_end = (elt_end+1)/32 * 32 /*-1*/;
  // std::size_t size_step = (elt_end - elt_start) / nbthreads;

  for( std::size_t iel = elt_start + idx ; iel <= elt_end - ((nbthreads-1)-idx) ; iel += nbthreads )
  // for( std::size_t iel = elt_start + idx * size_step ; iel <= elt_start + (idx+1) * size_step -1; ++iel )
  // for( std::size_t iel = elt_start + idx ; iel <= 0 ; iel += nbthreads )
  {

    // for( std::size_t i = 0 ; i < 5*5*5*3 + (5*5*5*3)/32 ; ++i ) {
    //   rl_displacement_gll[i] = 1.0;
    // }

    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          // printf("iel = %d : %ld\n", iel, 0 + 3 * IDX3( m, l, k ) + ((0 + 3 * IDX3( m, l, k )))/32 );
          // printf("iel = %d : %ld\n", iel, 1 + 3 * IDX3( m, l, k ) + ((1 + 3 * IDX3( m, l, k )))/32 );
          // printf("iel = %d : %ld\n", iel, 2 + 3 * IDX3( m, l, k ) + ((2 + 3 * IDX3( m, l, k )))/32 );

          auto igll = ig_hexa_gll_glonum[ IDX4( m, l, k, iel ) ];
          rl_displacement_gll[ 0 + 3 * IDX3( m, l, k ) + ((0 + 3 * IDX3( m, l, k )))/32 ] = rg_gll_displacement[ 0 + 3 * igll ];
          rl_displacement_gll[ 1 + 3 * IDX3( m, l, k ) + ((1 + 3 * IDX3( m, l, k )))/32 ] = rg_gll_displacement[ 1 + 3 * igll ];
          rl_displacement_gll[ 2 + 3 * IDX3( m, l, k ) + ((2 + 3 * IDX3( m, l, k )))/32 ] = rg_gll_displacement[ 2 + 3 * igll ];

        }
      }
    }

    // for( std::size_t i = 0 ; i < 5*5*5*3 + (5*5*5*3)/32 ; ++i ) {
    //   if( i%32 == 0 && i != 0)
    //     printf("\n");
    //   printf("%6.3f  ", rl_displacement_gll[i]);
    // }
    // printf("\n");

    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          // printf("iel = %ld : %ld\n", iel, IDX3X3(0,0,l,k));
          // printf("iel = %ld : %ld\n", iel, IDX3X3(0,1,l,k));
          // printf("iel = %ld : %ld\n", iel, IDX3X3(0,2,l,k));
          // printf("iel = %ld : %ld\n", iel, IDX3X3(0,3,l,k));
          // printf("iel = %ld : %ld\n", iel, IDX3X3(0,4,l,k));



               auto duxdxi = rl_displacement_gll[IDX3X3PAD(0,0,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,m)]
                       + rl_displacement_gll[IDX3X3PAD(0,1,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,m)]
                       + rl_displacement_gll[IDX3X3PAD(0,2,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,m)]
                       + rl_displacement_gll[IDX3X3PAD(0,3,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,m)]
                       + rl_displacement_gll[IDX3X3PAD(0,4,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,m)];

               auto duydxi = rl_displacement_gll[ IDX3X3PAD(1,0,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,m)]
                       + rl_displacement_gll[ IDX3X3PAD(1,1,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,m)]
                       + rl_displacement_gll[ IDX3X3PAD(1,2,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,m)]
                       + rl_displacement_gll[ IDX3X3PAD(1,3,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,m)]
                       + rl_displacement_gll[ IDX3X3PAD(1,4,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,m)];

               auto duzdxi = rl_displacement_gll[ IDX3X3PAD(2,0,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,m)]
                       + rl_displacement_gll[ IDX3X3PAD(2,1,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,m)]
                       + rl_displacement_gll[ IDX3X3PAD(2,2,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,m)]
                       + rl_displacement_gll[ IDX3X3PAD(2,3,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,m)]
                       + rl_displacement_gll[ IDX3X3PAD(2,4,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,m)];

               auto duxdet = rl_displacement_gll[ IDX3X3PAD(0,m,0,k) ]*rg_gll_lagrange_deriv_shared[IDX2(0,l)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,1,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,l)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,2,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,l)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,3,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,l)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,4,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,l)];

               auto duydet = rl_displacement_gll[ IDX3X3PAD(1,m,0,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,l)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,1,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,l)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,2,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,l)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,3,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,l)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,4,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,l)];

               auto duzdet = rl_displacement_gll[ IDX3X3PAD(2,m,0,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,l)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,1,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,l)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,2,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,l)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,3,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,l)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,4,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,l)];

               auto duxdze = rl_displacement_gll[ IDX3X3PAD(0,m,l,0)]*rg_gll_lagrange_deriv_shared[IDX2(0,k)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,l,1)]*rg_gll_lagrange_deriv_shared[IDX2(1,k)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,l,2)]*rg_gll_lagrange_deriv_shared[IDX2(2,k)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,l,3)]*rg_gll_lagrange_deriv_shared[IDX2(3,k)]
                       + rl_displacement_gll[ IDX3X3PAD(0,m,l,4)]*rg_gll_lagrange_deriv_shared[IDX2(4,k)];

               auto duydze = rl_displacement_gll[ IDX3X3PAD(1,m,l,0)]*rg_gll_lagrange_deriv_shared[IDX2(0,k)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,l,1)]*rg_gll_lagrange_deriv_shared[IDX2(1,k)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,l,2)]*rg_gll_lagrange_deriv_shared[IDX2(2,k)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,l,3)]*rg_gll_lagrange_deriv_shared[IDX2(3,k)]
                       + rl_displacement_gll[ IDX3X3PAD(1,m,l,4)]*rg_gll_lagrange_deriv_shared[IDX2(4,k)];

               auto duzdze = rl_displacement_gll[ IDX3X3PAD(2,m,l,0)]*rg_gll_lagrange_deriv_shared[IDX2(0,k)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,l,1)]*rg_gll_lagrange_deriv_shared[IDX2(1,k)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,l,2)]*rg_gll_lagrange_deriv_shared[IDX2(2,k)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,l,3)]*rg_gll_lagrange_deriv_shared[IDX2(3,k)]
                       + rl_displacement_gll[ IDX3X3PAD(2,m,l,4)]*rg_gll_lagrange_deriv_shared[IDX2(4,k)];


          //

          auto dxidx = rg_hexa_gll_dxidx[ IDX4( m, l, k, iel ) ];
          auto detdx = rg_hexa_gll_detdx[ IDX4( m, l, k, iel ) ];
          auto dzedx = rg_hexa_gll_dzedx[ IDX4( m, l, k, iel ) ];

          auto duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx;
          auto duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx;
          auto duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx;

          auto dxidy = rg_hexa_gll_dxidy[ IDX4( m, l, k, iel ) ];
          auto detdy = rg_hexa_gll_detdy[ IDX4( m, l, k, iel ) ];
          auto dzedy = rg_hexa_gll_dzedy[ IDX4( m, l, k, iel ) ];

          auto duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy;
          auto duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy;
          auto duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy;

          auto dxidz = rg_hexa_gll_dxidz[ IDX4( m, l, k, iel ) ];
          auto detdz = rg_hexa_gll_detdz[ IDX4( m, l, k, iel ) ];
          auto dzedz = rg_hexa_gll_dzedz[ IDX4( m, l, k, iel ) ];

          auto duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz;
          auto duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz;
          auto duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz;

          // Load 8 values from rg_hexa_gll_rhovp2 and rg_hexa_gll_rhovs2.
          auto rhovp2 = rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel ) ];
          auto rhovs2 = rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel ) ];

          auto trace_tau = ( rhovp2 - 2.0f * rhovs2 )*(duxdx+duydy+duzdz);
          auto tauxx     = trace_tau + 2.0f*rhovs2*duxdx;
          auto tauyy     = trace_tau + 2.0f*rhovs2*duydy;
          auto tauzz     = trace_tau + 2.0f*rhovs2*duzdz;
          auto tauxy     =                 rhovs2*(duxdy+duydx);
          auto tauxz     =                 rhovs2*(duxdz+duzdx);
          auto tauyz     =                 rhovs2*(duydz+duzdy);

          intpx1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
          intpx2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*detdx+tauxy*detdy+tauxz*detdz);
          intpx3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

          intpy1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
          intpy2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*detdx+tauyy*detdy+tauyz*detdz);
          intpy3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

          intpz1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
          intpz2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*detdx+tauyz*detdy+tauzz*detdz);
          intpz3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
        }
      }
    }
    
    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          float tmpx1 = intpx1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpx1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpx1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpx1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpx1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpy1 = intpy1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpy1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpy1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpy1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpy1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpz1 = intpz1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpz1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpz1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpz1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpz1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpx2 = intpx2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpx2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpx2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpx2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpx2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpy2 = intpy2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpy2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpy2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpy2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpy2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpz2 = intpz2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpz2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpz2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpz2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpz2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpx3 = intpx3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpx3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpx3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpx3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpx3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpy3 = intpy3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpy3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpy3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpy3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpy3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpz3 = intpz3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpz3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpz3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpz3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpz3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float fac1 = rg_gll_weight_shared[ l ]*rg_gll_weight_shared[ k ];
          float fac2 = rg_gll_weight_shared[ m ]*rg_gll_weight_shared[ k ];
          float fac3 = rg_gll_weight_shared[ m ]*rg_gll_weight_shared[ l ];

          uint32_t index = ig_hexa_gll_glonum[ IDX4( m, l, k, iel ) ]/* - 1 */;

          rg_gll_acceleration[ 0 + 3 * index ] -= fac1 * tmpx1 + fac2 * tmpx2 + fac3 * tmpx3;
          rg_gll_acceleration[ 1 + 3 * index ] -= fac1 * tmpy1 + fac2 * tmpy2 + fac3 * tmpy3;
          rg_gll_acceleration[ 2 + 3 * index ] -= fac1 * tmpz1 + fac2 * tmpz2 + fac3 * tmpz3;

        }
      }
    }
    
  }


  ////////////////// tail


  if ( idx == 0 ) {

    for( std::size_t iel = elt_32_end ; iel <= elt_end-1 ; ++iel )
    {

      for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {

          auto igll = ig_hexa_gll_glonum[ IDX4( m, l, k, iel ) ];
          rl_displacement_gll[ 0 + 3 * IDX3( m, l, k ) /*+ ((0 + 3 * IDX3( m, l, k )))/32*/ ] = rg_gll_displacement[ 0 + 3 * igll ];
          rl_displacement_gll[ 1 + 3 * IDX3( m, l, k ) /*+ ((1 + 3 * IDX3( m, l, k )))/32*/ ] = rg_gll_displacement[ 1 + 3 * igll ];
          rl_displacement_gll[ 2 + 3 * IDX3( m, l, k ) /*+ ((2 + 3 * IDX3( m, l, k )))/32*/ ] = rg_gll_displacement[ 2 + 3 * igll ];

        }
      }
    }

    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {

               auto duxdxi = rl_displacement_gll[IDX3X3(0,0,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,m)]
                       + rl_displacement_gll[IDX3X3(0,1,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,m)]
                       + rl_displacement_gll[IDX3X3(0,2,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,m)]
                       + rl_displacement_gll[IDX3X3(0,3,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,m)]
                       + rl_displacement_gll[IDX3X3(0,4,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,m)];

               auto duydxi = rl_displacement_gll[ IDX3X3(1,0,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,m)]
                       + rl_displacement_gll[ IDX3X3(1,1,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,m)]
                       + rl_displacement_gll[ IDX3X3(1,2,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,m)]
                       + rl_displacement_gll[ IDX3X3(1,3,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,m)]
                       + rl_displacement_gll[ IDX3X3(1,4,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,m)];

               auto duzdxi = rl_displacement_gll[ IDX3X3(2,0,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,m)]
                       + rl_displacement_gll[ IDX3X3(2,1,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,m)]
                       + rl_displacement_gll[ IDX3X3(2,2,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,m)]
                       + rl_displacement_gll[ IDX3X3(2,3,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,m)]
                       + rl_displacement_gll[ IDX3X3(2,4,l,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,m)];

               auto duxdet = rl_displacement_gll[ IDX3X3(0,m,0,k) ]*rg_gll_lagrange_deriv_shared[IDX2(0,l)]
                       + rl_displacement_gll[ IDX3X3(0,m,1,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,l)]
                       + rl_displacement_gll[ IDX3X3(0,m,2,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,l)]
                       + rl_displacement_gll[ IDX3X3(0,m,3,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,l)]
                       + rl_displacement_gll[ IDX3X3(0,m,4,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,l)];

               auto duydet = rl_displacement_gll[ IDX3X3(1,m,0,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,l)]
                       + rl_displacement_gll[ IDX3X3(1,m,1,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,l)]
                       + rl_displacement_gll[ IDX3X3(1,m,2,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,l)]
                       + rl_displacement_gll[ IDX3X3(1,m,3,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,l)]
                       + rl_displacement_gll[ IDX3X3(1,m,4,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,l)];

               auto duzdet = rl_displacement_gll[ IDX3X3(2,m,0,k)]*rg_gll_lagrange_deriv_shared[IDX2(0,l)]
                       + rl_displacement_gll[ IDX3X3(2,m,1,k)]*rg_gll_lagrange_deriv_shared[IDX2(1,l)]
                       + rl_displacement_gll[ IDX3X3(2,m,2,k)]*rg_gll_lagrange_deriv_shared[IDX2(2,l)]
                       + rl_displacement_gll[ IDX3X3(2,m,3,k)]*rg_gll_lagrange_deriv_shared[IDX2(3,l)]
                       + rl_displacement_gll[ IDX3X3(2,m,4,k)]*rg_gll_lagrange_deriv_shared[IDX2(4,l)];

               auto duxdze = rl_displacement_gll[ IDX3X3(0,m,l,0)]*rg_gll_lagrange_deriv_shared[IDX2(0,k)]
                       + rl_displacement_gll[ IDX3X3(0,m,l,1)]*rg_gll_lagrange_deriv_shared[IDX2(1,k)]
                       + rl_displacement_gll[ IDX3X3(0,m,l,2)]*rg_gll_lagrange_deriv_shared[IDX2(2,k)]
                       + rl_displacement_gll[ IDX3X3(0,m,l,3)]*rg_gll_lagrange_deriv_shared[IDX2(3,k)]
                       + rl_displacement_gll[ IDX3X3(0,m,l,4)]*rg_gll_lagrange_deriv_shared[IDX2(4,k)];

               auto duydze = rl_displacement_gll[ IDX3X3(1,m,l,0)]*rg_gll_lagrange_deriv_shared[IDX2(0,k)]
                       + rl_displacement_gll[ IDX3X3(1,m,l,1)]*rg_gll_lagrange_deriv_shared[IDX2(1,k)]
                       + rl_displacement_gll[ IDX3X3(1,m,l,2)]*rg_gll_lagrange_deriv_shared[IDX2(2,k)]
                       + rl_displacement_gll[ IDX3X3(1,m,l,3)]*rg_gll_lagrange_deriv_shared[IDX2(3,k)]
                       + rl_displacement_gll[ IDX3X3(1,m,l,4)]*rg_gll_lagrange_deriv_shared[IDX2(4,k)];

               auto duzdze = rl_displacement_gll[ IDX3X3(2,m,l,0)]*rg_gll_lagrange_deriv_shared[IDX2(0,k)]
                       + rl_displacement_gll[ IDX3X3(2,m,l,1)]*rg_gll_lagrange_deriv_shared[IDX2(1,k)]
                       + rl_displacement_gll[ IDX3X3(2,m,l,2)]*rg_gll_lagrange_deriv_shared[IDX2(2,k)]
                       + rl_displacement_gll[ IDX3X3(2,m,l,3)]*rg_gll_lagrange_deriv_shared[IDX2(3,k)]
                       + rl_displacement_gll[ IDX3X3(2,m,l,4)]*rg_gll_lagrange_deriv_shared[IDX2(4,k)];


          //

          auto dxidx = rg_hexa_gll_dxidx[ IDX4( m, l, k, iel ) ];
          auto detdx = rg_hexa_gll_detdx[ IDX4( m, l, k, iel ) ];
          auto dzedx = rg_hexa_gll_dzedx[ IDX4( m, l, k, iel ) ];

          auto duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx;
          auto duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx;
          auto duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx;

          auto dxidy = rg_hexa_gll_dxidy[ IDX4( m, l, k, iel ) ];
          auto detdy = rg_hexa_gll_detdy[ IDX4( m, l, k, iel ) ];
          auto dzedy = rg_hexa_gll_dzedy[ IDX4( m, l, k, iel ) ];

          auto duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy;
          auto duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy;
          auto duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy;

          auto dxidz = rg_hexa_gll_dxidz[ IDX4( m, l, k, iel ) ];
          auto detdz = rg_hexa_gll_detdz[ IDX4( m, l, k, iel ) ];
          auto dzedz = rg_hexa_gll_dzedz[ IDX4( m, l, k, iel ) ];

          auto duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz;
          auto duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz;
          auto duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz;

          // Load 8 values from rg_hexa_gll_rhovp2 and rg_hexa_gll_rhovs2.
          auto rhovp2 = rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel ) ];
          auto rhovs2 = rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel ) ];

          auto trace_tau = ( rhovp2 - 2.0f * rhovs2 )*(duxdx+duydy+duzdz);
          auto tauxx     = trace_tau + 2.0f*rhovs2*duxdx;
          auto tauyy     = trace_tau + 2.0f*rhovs2*duydy;
          auto tauzz     = trace_tau + 2.0f*rhovs2*duzdz;
          auto tauxy     =                 rhovs2*(duxdy+duydx);
          auto tauxz     =                 rhovs2*(duxdz+duzdx);
          auto tauyz     =                 rhovs2*(duydz+duzdy);

          intpx1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
          intpx2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*detdx+tauxy*detdy+tauxz*detdz);
          intpx3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

          intpy1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
          intpy2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*detdx+tauyy*detdy+tauyz*detdz);
          intpy3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

          intpz1[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
          intpz2[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*detdx+tauyz*detdy+tauzz*detdz);
          intpz3[ IDX3( m, l, k ) ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel ) ]*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
        }
      }
    }
    
    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          float tmpx1 = intpx1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpx1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpx1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpx1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpx1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpy1 = intpy1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpy1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpy1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpy1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpy1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpz1 = intpz1[ IDX3( 0, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpz1[ IDX3( 1, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpz1[ IDX3( 2, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpz1[ IDX3( 3, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpz1[ IDX3( 4, l, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( m, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpx2 = intpx2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpx2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpx2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpx2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpx2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpy2 = intpy2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpy2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpy2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpy2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpy2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpz2 = intpz2[ IDX3( m, 0, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpz2[ IDX3( m, 1, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpz2[ IDX3( m, 2, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpz2[ IDX3( m, 3, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpz2[ IDX3( m, 4, k ) ] * rg_gll_lagrange_deriv_shared[ IDX2( l, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpx3 = intpx3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpx3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpx3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpx3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpx3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpy3 = intpy3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpy3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpy3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpy3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpy3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float tmpz3 = intpz3[ IDX3( m, l, 0 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 0 ) ]*rg_gll_weight_shared[ 0 ]
                     + intpz3[ IDX3( m, l, 1 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 1 ) ]*rg_gll_weight_shared[ 1 ]
                     + intpz3[ IDX3( m, l, 2 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 2 ) ]*rg_gll_weight_shared[ 2 ]
                     + intpz3[ IDX3( m, l, 3 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 3 ) ]*rg_gll_weight_shared[ 3 ]
                     + intpz3[ IDX3( m, l, 4 ) ] * rg_gll_lagrange_deriv_shared[ IDX2( k, 4 ) ]*rg_gll_weight_shared[ 4 ];

          float fac1 = rg_gll_weight_shared[ l ]*rg_gll_weight_shared[ k ];
          float fac2 = rg_gll_weight_shared[ m ]*rg_gll_weight_shared[ k ];
          float fac3 = rg_gll_weight_shared[ m ]*rg_gll_weight_shared[ l ];

          uint32_t index = ig_hexa_gll_glonum[ IDX4( m, l, k, iel ) ]/* - 1 */;

          rg_gll_acceleration[ 0 + 3 * index ] -= fac1 * tmpx1 + fac2 * tmpx2 + fac3 * tmpx3;
          rg_gll_acceleration[ 1 + 3 * index ] -= fac1 * tmpy1 + fac2 * tmpy2 + fac3 * tmpy3;
          rg_gll_acceleration[ 2 + 3 * index ] -= fac1 * tmpz1 + fac2 * tmpz2 + fac3 * tmpz3;

        }
      }
    }
      
    }

  }
    
}


