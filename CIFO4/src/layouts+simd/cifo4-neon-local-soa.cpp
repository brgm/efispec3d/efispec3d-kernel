#include <cstddef>
#include <iostream>

#include <arm_neon.h>

#include <cifo4.hpp>


#define IDX2( m, l ) ( 5 * l + m )
#define IDX3( m, l, k ) ( 25 * k + 5 * l + m )
#define IDX4( m, l, k, iel ) ( 125 * (iel) + 25 * (k) + 5 * (l) + (m) )


veci ig_hexa_gll_glonum;

vecf rg_gll_displacement;
vecf rg_gll_displacement_soa;
vecf rg_gll_weight;

vecf rg_gll_lagrange_deriv;
vecf rg_gll_acceleration;

vecf rg_hexa_gll_dxidx;
vecf rg_hexa_gll_dxidy;
vecf rg_hexa_gll_dxidz;
vecf rg_hexa_gll_detdx;
vecf rg_hexa_gll_detdy;
vecf rg_hexa_gll_detdz;
vecf rg_hexa_gll_dzedx;
vecf rg_hexa_gll_dzedy;
vecf rg_hexa_gll_dzedz;

vecf rg_hexa_gll_rhovp2;
vecf rg_hexa_gll_rhovs2;
vecf rg_hexa_gll_jacobian_det;


void compute_internal_forces_order4( std::size_t elt_start, std::size_t elt_end )
{
  float32x4_t rl_displacement_gll[5*5*5*3];

  float32x4_t local[ 5 * 5 * 5 * 9 ];

  float32x4_t * intpx1 = &local[    0 ];
  float32x4_t * intpy1 = &local[  125 ];
  float32x4_t * intpz1 = &local[  250 ];

  float32x4_t * intpx2 = &local[  375 ];
  float32x4_t * intpy2 = &local[  500 ];
  float32x4_t * intpz2 = &local[  625 ];

  float32x4_t * intpx3 = &local[  750 ];
  float32x4_t * intpy3 = &local[  875 ];
  float32x4_t * intpz3 = &local[ 1000 ];

  for( std::size_t iel = elt_start ; iel < elt_end ; iel+=4 )
  {
    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
	  auto idx = vld1q_u32( &ig_hexa_gll_glonum[ iel + elt_end * IDX3( m, l, k ) ] );
	  auto idx_x = vmulq_u32( idx, vdupq_n_u32( 3 ) );
	  auto idx_y = vaddq_u32( idx_x, vdupq_n_u32( 1 ) );
	  auto idx_z = vaddq_u32( idx_x, vdupq_n_u32( 2 ) );

	  rl_displacement_gll[ 0 + 3 * IDX3( m, l, k ) ] = _mm256_i32gather_ps( rg_gll_displacement.data(), idx_x, 4 );
	  rl_displacement_gll[ 1 + 3 * IDX3( m, l, k ) ] = _mm256_i32gather_ps( rg_gll_displacement.data(), idx_y, 4 );
	  rl_displacement_gll[ 2 + 3 * IDX3( m, l, k ) ] = _mm256_i32gather_ps( rg_gll_displacement.data(), idx_z, 4 );

/*
          rl_displacement_gll[ 0 + 3 * IDX3( m, l, k ) ] = _mm256_setr_ps( rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 0 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 1 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 2 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 3 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 4 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 5 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 6 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 7 ) ) ] ) ] );

          rl_displacement_gll[ 1 + 3 * IDX3( m, l, k ) ] = _mm256_setr_ps( rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 0 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 1 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 2 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 3 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 4 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 5 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 6 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 7 ) ) ] ) ] );

          rl_displacement_gll[ 2 + 3 * IDX3( m, l, k ) ] = _mm256_setr_ps( rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 0 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 1 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 2 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 3 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 4 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 5 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 6 ) ) ] ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 7 ) ) ] ) ] );
*/
        }
      }
    }

    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {
          auto coeff = vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 0, m ) ] );

          auto index = 0 + 3 * IDX3( 0, l, k );

          auto duxdxi = rl_displacement_gll[ 0 + index ] * coeff;
          auto duydxi = rl_displacement_gll[ 1 + index ] * coeff;
          auto duzdxi = rl_displacement_gll[ 2 + index ] * coeff;

          coeff = vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 1, m ) ] );

          duxdxi += rl_displacement_gll[ 3 + index ] * coeff;
          duydxi += rl_displacement_gll[ 4 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 5 + index ] * coeff;

          coeff = vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 2, m ) ] );

          duxdxi += rl_displacement_gll[ 6 + index ] * coeff;
          duydxi += rl_displacement_gll[ 7 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 8 + index ] * coeff;

          coeff = vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 3, m ) ] );

          duxdxi += rl_displacement_gll[  9 + index ] * coeff;
          duydxi += rl_displacement_gll[ 10 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 11 + index ] * coeff;

          coeff = vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 4, m ) ] );

          duxdxi += rl_displacement_gll[ 12 + index ] * coeff;
          duydxi += rl_displacement_gll[ 13 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 14 + index ] * coeff;

          //

          coeff = vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 0, l ) ] );

          auto duxdet = rl_displacement_gll[ 0 + 3 * IDX3( m, 0, k ) ] * coeff;
          auto duydet = rl_displacement_gll[ 1 + 3 * IDX3( m, 0, k ) ] * coeff;
          auto duzdet = rl_displacement_gll[ 2 + 3 * IDX3( m, 0, k ) ] * coeff;

          coeff = vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 1, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 1, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 1, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 1, k ) ] * coeff;

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 2, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 2, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 2, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 2, k ) ] * coeff;

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 3, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 3, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 3, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 3, k ) ] * coeff;

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 4, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 4, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 4, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 4, k ) ] * coeff;

          //

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 0, k ) ] );

          auto duxdze = rl_displacement_gll[ 0 + 3 * IDX3( m, l, 0 ) ] * coeff;
          auto duydze = rl_displacement_gll[ 1 + 3 * IDX3( m, l, 0 ) ] * coeff;
          auto duzdze = rl_displacement_gll[ 2 + 3 * IDX3( m, l, 0 ) ] * coeff;

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 1, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 1 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 1 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 1 ) ] * coeff;

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 2, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 2 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 2 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 2 ) ] * coeff;

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 3, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 3 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 3 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 3 ) ] * coeff;

          coeff =vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( 4, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 4 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 4 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 4 ) ] * coeff;

	  auto const i = iel + elt_end * IDX3( m, l, k );

          //
          auto dxidx = vld1q_f32( &(rg_hexa_gll_dxidx[ i ]) );
	        auto detdx = vld1q_f32( &(rg_hexa_gll_detdx[ i ]) );
	        auto dzedx = vld1q_f32( &(rg_hexa_gll_dzedx[ i ]) );

          auto duxdx = duxdxi * dxidx + duxdet * detdx + duxdze * dzedx;
          auto duydx = duydxi * dxidx + duydet * detdx + duydze * dzedx;
          auto duzdx = duzdxi * dxidx + duzdet * detdx + duzdze * dzedx;

          auto dxidy = vld1q_f32( &(rg_hexa_gll_dxidy[ i ]) );
	        auto detdy = vld1q_f32( &(rg_hexa_gll_detdy[ i ]) );
          auto dzedy = vld1q_f32( &(rg_hexa_gll_dzedy[ i ]) );

          auto duxdy = duxdxi * dxidy + duxdet * detdy + duxdze * dzedy;
          auto duydy = duydxi * dxidy + duydet * detdy + duydze * dzedy;
          auto duzdy = duzdxi * dxidy + duzdet * detdy + duzdze * dzedy;

          auto dxidz = vld1q_f32( &(rg_hexa_gll_dxidz[ i ]) );
	        auto detdz = vld1q_f32( &(rg_hexa_gll_detdz[ i ]) );
	        auto dzedz = vld1q_f32( &(rg_hexa_gll_dzedz[ i ]) );

          auto duxdz = duxdxi * dxidz + duxdet * detdz + duxdze * dzedz;
          auto duydz = duydxi * dxidz + duydet * detdz + duydze * dzedz;
          auto duzdz = duzdxi * dxidz + duzdet * detdz + duzdze * dzedz;

	        auto rhovp2 = vld1q_f32( &(rg_hexa_gll_rhovp2[ i ]) );
          auto rhovs2 = vld1q_f32( &(rg_hexa_gll_rhovs2[ i ]) );

          auto trace_tau = ( rhovp2 -vdupq_n_f32( 2.0 ) * rhovs2 )*(duxdx+duydy+duzdz);
          auto tauxx     = trace_tau +vdupq_n_f32( 2.0 )*rhovs2*duxdx;
          auto tauyy     = trace_tau +vdupq_n_f32( 2.0 )*rhovs2*duydy;
          auto tauzz     = trace_tau +vdupq_n_f32( 2.0 )*rhovs2*duzdz;
          auto tauxy     =                 rhovs2*(duxdy+duydx);
          auto tauxz     =                 rhovs2*(duxdz+duzdx);
          auto tauyz     =                 rhovs2*(duydz+duzdy);

	  auto tmp = vld1q_f32( &rg_hexa_gll_jacobian_det[ i ] );

          intpx1[ IDX3( m, l, k ) ] = tmp * (tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
          intpx2[ IDX3( m, l, k ) ] = tmp * (tauxx*detdx+tauxy*detdy+tauxz*detdz);
          intpx3[ IDX3( m, l, k ) ] = tmp * (tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

          intpy1[ IDX3( m, l, k ) ] = tmp * (tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
          intpy2[ IDX3( m, l, k ) ] = tmp * (tauxy*detdx+tauyy*detdy+tauyz*detdz);
          intpy3[ IDX3( m, l, k ) ] = tmp * (tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

          intpz1[ IDX3( m, l, k ) ] = tmp * (tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
          intpz2[ IDX3( m, l, k ) ] = tmp * (tauxz*detdx+tauyz*detdy+tauzz*detdz);
          intpz3[ IDX3( m, l, k ) ] = tmp * (tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
        }
      }
    }

    for( std::size_t k = 0 ; k < 5 ; ++k )
    {
      for( std::size_t l = 0 ; l < 5 ; ++l )
      {
        for( std::size_t m = 0 ; m < 5 ; ++m )
        {

	  auto tmpx1 = intpx1[ IDX3( 0, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpx1[ IDX3( 1, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpx1[ IDX3( 2, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpx1[ IDX3( 3, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpx1[ IDX3( 4, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpy1 = intpy1[ IDX3( 0, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpy1[ IDX3( 1, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpy1[ IDX3( 2, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpy1[ IDX3( 3, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpy1[ IDX3( 4, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpz1 = intpz1[ IDX3( 0, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpz1[ IDX3( 1, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpz1[ IDX3( 2, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpz1[ IDX3( 3, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpz1[ IDX3( 4, l, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpx2 = intpx2[ IDX3( m, 0, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpx2[ IDX3( m, 1, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpx2[ IDX3( m, 2, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpx2[ IDX3( m, 3, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpx2[ IDX3( m, 4, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpy2 = intpy2[ IDX3( m, 0, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpy2[ IDX3( m, 1, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpy2[ IDX3( m, 2, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpy2[ IDX3( m, 3, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpy2[ IDX3( m, 4, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpz2 = intpz2[ IDX3( m, 0, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpz2[ IDX3( m, 1, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpz2[ IDX3( m, 2, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpz2[ IDX3( m, 3, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpz2[ IDX3( m, 4, k ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpx3 = intpx3[ IDX3( m, l, 0 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpx3[ IDX3( m, l, 1 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpx3[ IDX3( m, l, 2 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpx3[ IDX3( m, l, 3 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpx3[ IDX3( m, l, 4 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpy3 = intpy3[ IDX3( m, l, 0 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpy3[ IDX3( m, l, 1 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpy3[ IDX3( m, l, 2 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpy3[ IDX3( m, l, 3 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpy3[ IDX3( m, l, 4 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

          auto tmpz3 = intpz3[ IDX3( m, l, 0 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) *vdupq_n_f32( rg_gll_weight[ 0 ] )
                     + intpz3[ IDX3( m, l, 1 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) *vdupq_n_f32( rg_gll_weight[ 1 ] )
                     + intpz3[ IDX3( m, l, 2 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) *vdupq_n_f32( rg_gll_weight[ 2 ] )
                     + intpz3[ IDX3( m, l, 3 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) *vdupq_n_f32( rg_gll_weight[ 3 ] )
                     + intpz3[ IDX3( m, l, 4 ) ] *vdupq_n_f32( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) *vdupq_n_f32( rg_gll_weight[ 4 ] );

    auto fac1 =vdupq_n_f32( rg_gll_weight[ l ] ) *vdupq_n_f32( rg_gll_weight[ k ] );
          auto fac2 =vdupq_n_f32( rg_gll_weight[ m ] ) *vdupq_n_f32( rg_gll_weight[ k ] );
          auto fac3 =vdupq_n_f32( rg_gll_weight[ m ] ) *vdupq_n_f32( rg_gll_weight[ l ] );

          auto rx = fac1 * tmpx1 + fac2 * tmpx2 + fac3 * tmpx3;
          auto ry = fac1 * tmpy1 + fac2 * tmpy2 + fac3 * tmpy3;
          auto rz = fac1 * tmpz1 + fac2 * tmpz2 + fac3 * tmpz3;

          float tx[ 8 ] __attribute__((aligned(16)));//alignas(32);
          float ty[ 8 ] __attribute__((aligned(16)));//alignas(32);
          float tz[ 8 ] __attribute__((aligned(16)));//alignas(32);
          vst1q_f32( tx, rx );
          vst1q_f32( ty, ry );
          vst1q_f32( tz, rz );

          for( std::size_t i = 0 ; i < 8 ; ++i )
          {
            auto idx = ig_hexa_gll_glonum[ iel + i + elt_end * IDX3( m, l, k ) ];

            rg_gll_acceleration[ 0 + 3 * idx ] -= tx[ i ];
            rg_gll_acceleration[ 1 + 3 * idx ] -= ty[ i ];
            rg_gll_acceleration[ 2 + 3 * idx ] -= tz[ i ];
          }
        }
      }
    }
  }
}
