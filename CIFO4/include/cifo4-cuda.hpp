#include <vector>
#include <cstdint>

#include <boost/align/aligned_allocator.hpp>


using vecf = std::vector< float, boost::alignment::aligned_allocator< float, 64 > >;
using veci = std::vector< uint32_t, boost::alignment::aligned_allocator< uint32_t, 64 > >;

extern veci ig_hexa_gll_glonum;

extern vecf rg_gll_displacement;
extern vecf rg_gll_displacement_hyb;
extern vecf rg_gll_displacement_soa;


extern vecf rg_gll_weight;
extern vecf rg_gll_lagrange_deriv;
extern vecf rg_gll_acceleration;

extern vecf rg_hexa_gll_dxidx;
extern vecf rg_hexa_gll_dxidy;
extern vecf rg_hexa_gll_dxidz;
extern vecf rg_hexa_gll_detdx;
extern vecf rg_hexa_gll_detdy;
extern vecf rg_hexa_gll_detdz;
extern vecf rg_hexa_gll_dzedx;
extern vecf rg_hexa_gll_dzedy;
extern vecf rg_hexa_gll_dzedz;

// HYB
extern vecf rg_hexa_gll_dxi;
extern vecf rg_hexa_gll_det;
extern vecf rg_hexa_gll_dze;
//

extern vecf rg_hexa_gll_rhovp2;
extern vecf rg_hexa_gll_rhovs2;
extern vecf rg_hexa_gll_jacobian_det;

// VISCO
extern vecf rg_mem_var_exp;
extern vecf rg_hexa_gll_wkqp;
extern vecf rg_hexa_gll_wkqs;
extern vecf rg_hexa_gll_ksixx;
extern vecf rg_hexa_gll_ksiyy;
extern vecf rg_hexa_gll_ksizz;
extern vecf rg_hexa_gll_ksixy;
extern vecf rg_hexa_gll_ksixz;
extern vecf rg_hexa_gll_ksiyz;


__global__ void compute_internal_forces_order4( std::size_t elt_start, std::size_t elt_end, uint32_t * ig_hexa_gll_glonum, float * rg_gll_displacement, float * rg_gll_lagrange_deriv, float * rg_hexa_gll_dxidx, float * rg_hexa_gll_dxidy, float * rg_hexa_gll_dxidz, float * rg_hexa_gll_detdx, float * rg_hexa_gll_detdy, float * rg_hexa_gll_detdz, float * rg_hexa_gll_dzedx, float * rg_hexa_gll_dzedy, float * rg_hexa_gll_dzedz, float * rg_hexa_gll_rhovp2, float * rg_hexa_gll_rhovs2, float * rg_hexa_gll_jacobian_det, float * rg_gll_weight, float * rg_gll_acceleration);