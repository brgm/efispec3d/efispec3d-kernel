
module mod_solver
  !
  use mpi
  implicit none
  !
contains
  !
  !***************************************************
  !subroutine newmark_ini
  !subroutine newmark_end
  !subroutine compute_internal_forces_order4
  !subroutine compute_internal_forces_order5
  !subroutine compute_internal_forces_order6
  !subroutine compute_absorption_forces
  !***************************************************

  !
  !*****************************************************************
  subroutine compute_internal_forces_order4(&
       ig_global_gll_of_hexa,&
       rg_derivative_xi_x_of_hexa  ,&
       rg_derivative_xi_y_of_hexa  ,&
       rg_derivative_xi_z_of_hexa  ,&
       rg_derivative_eta_x_of_hexa ,&
       rg_derivative_eta_y_of_hexa ,&
       rg_derivative_eta_z_of_hexa ,&
       rg_derivative_zeta_x_of_hexa,&
       rg_derivative_zeta_y_of_hexa,&
       rg_derivative_zeta_z_of_hexa,&
       rg_rhovp2_gll,&
       rg_rhovs2_gll,&
       rg_jacobian_determinant_of_hexa)

    !*****************************************************************
    use mpi
    use mod_global_variables, only :&
         IG_NGLL&
         ,rg_derivative_lagrange_poly&
         ,rg_displacement_gll_1,rg_displacement_gll_2,rg_displacement_gll_3&
         ,rg_acceleration_gll_1,rg_acceleration_gll_2,rg_acceleration_gll_3&
         ,rg_weight_gll&
         ,rg_abscissa_gll&
         ,ig_number_of_receiver_cpu&
         ,ig_current_time_step&
         ,ig_freq_write_rec_time_history&
         ,ig_number_of_hexa_cpu&
         ,ig_my_rank&
         ,rg_rho_gll&
         ,rg_wkqs_gll&
         ,rg_wkqp_gll&
         ,rg_memory_variables_ksixx&
         ,rg_memory_variables_ksiyy&
         ,rg_memory_variables_ksizz&
         ,rg_memory_variables_ksixy&
         ,rg_memory_variables_ksixz&
         ,rg_memory_variables_ksiyz&
         ,rg_coeff_memory_variables&
         ,ig_number_of_memory_var&
         ,rg_time_step&
         ,IS_VISCO&
         ,ig_number_of_hexa_cpu&
         ,prod1,prod2,prod3

    implicit none

    !     integer, intent(in)          :: elt_start,elt_end

    integer, parameter :: n3=ig_ngll*ig_ngll*ig_ngll
    integer, dimension(n3) ::          ig_global_gll_of_hexa
    real,dimension(n3) ::      &
         rg_derivative_xi_x_of_hexa  ,&
         rg_derivative_xi_y_of_hexa  ,&
         rg_derivative_xi_z_of_hexa  ,&
         rg_derivative_eta_x_of_hexa ,&
         rg_derivative_eta_y_of_hexa ,&
         rg_derivative_eta_z_of_hexa ,&
         rg_derivative_zeta_x_of_hexa,&
         rg_derivative_zeta_y_of_hexa,&
         rg_derivative_zeta_z_of_hexa,&
         rg_rhovp2_gll,&
         rg_rhovs2_gll,&
         rg_jacobian_determinant_of_hexa

    real    :: intpx1(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpx2(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpx3(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpy1(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpy2(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpy3(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpz1(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpz2(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpz3(ig_ngll*ig_ngll*ig_ngll) 

    real,dimension(n3) :: duxdxi,duxdet,duxdze,&
         duydxi,duydet,duydze,&
         duzdxi,duzdet,duzdze
    real    :: tmpx1,tmpx2,tmpx3,tmpy1,tmpy2,tmpy3,tmpz1,tmpz2,tmpz3,fac1,fac2,fac3


    real    :: duxdx,duxdy,duxdz
    real    :: duydx,duydy,duydz
    real    :: duzdx,duzdy,duzdz

    real    :: dxidx,dxidy,dxidz
    real    :: detdx,detdy,detdz
    real    :: dzedx,dzedy,dzedz
    real    :: tauxx,tauyy,tauzz
    real    :: tauxy,tauxz,tauyz

    real    :: tauxx_n12
    real    :: tauyy_n12
    real    :: tauzz_n12
    real    :: tauxy_n12
    real    :: tauxz_n12
    real    :: tauyz_n12
    real    :: trace_tau



    real,dimension(n3) :: rl_acceleration_gll_1,rl_acceleration_gll_2,rl_acceleration_gll_3
    real,dimension(n3) :: rl_displacement_gll_1,rl_displacement_gll_2,rl_displacement_gll_3

    integer :: ios
    integer :: iel
    integer :: i
    integer :: j
    integer :: k
    integer :: l
    integer :: m
    integer :: n
    integer :: igll
    integer :: imem_var
    integer :: i3,icpt,n_l_k,m_n_k,m_l_n
    integer :: n_m,n_l,n_k,m_n,l_n,k_n

    !
    !------->flush local acceleration to zero
!!DEC$ SIMD VECREMAINDER
!    do m=1,n3
!       rl_acceleration_gll_1(m) = 0.0
!    enddo
!!DEC$ SIMD VECREMAINDER
!    do m=1,n3
!       rl_acceleration_gll_2(m) = 0.0
!    enddo
!!DEC$ SIMD VECREMAINDER
!    do m=1,n3
!       rl_acceleration_gll_3(m) = 0.0
!    enddo

    !
    !------->fill local displacement
!DEC$ SIMD VECREMAINDER
!DEC$ VECTOR NONTEMPORAL (igll)
    do m = 1,n3
       igll = ig_global_gll_of_hexa(m)
       rl_displacement_gll_1(m) = rg_displacement_gll_1(igll)
       rl_displacement_gll_2(m) = rg_displacement_gll_2(igll)
       rl_displacement_gll_3(m) = rg_displacement_gll_3(igll)
    enddo
    !
    !******************************************************************************
    !->compute integrale at gll nodes + assemble force in global gll grid for hexa

    icpt=0
    do k = 1,ig_ngll        !zeta
       do l = 1,ig_ngll     !eta
          do m = 1,ig_ngll  !xi

             icpt = icpt + 1
             duxdxi(icpt) = 0.0
             duxdet(icpt) = 0.0
             duxdze(icpt) = 0.0
             duydxi(icpt) = 0.0
             duydet(icpt) = 0.0
             duydze(icpt) = 0.0
             duzdxi(icpt) = 0.0
             duzdet(icpt) = 0.0
             duzdze(icpt) = 0.0

             !DEC$ SIMD
             !DEC$ VECTOR NONTEMPORAL (n_l_k,n_m) 
             do n = 1,IG_NGLL
                n_l_k = (k-1)*(ig_ngll*ig_ngll) + (l-1)*ig_ngll + n
                n_m = (m-1) *ig_ngll + n
                duxdxi(icpt) = duxdxi(icpt) + rl_displacement_gll_1(n_l_k)*rg_derivative_lagrange_poly(n_m) 
                duydxi(icpt) = duydxi(icpt) + rl_displacement_gll_2(n_l_k)*rg_derivative_lagrange_poly(n_m) 
                duzdxi(icpt) = duzdxi(icpt) + rl_displacement_gll_3(n_l_k)*rg_derivative_lagrange_poly(n_m)

                m_n_k = (k-1)*(ig_ngll*ig_ngll) + (n-1)*ig_ngll + m
                n_l = (l-1) *ig_ngll + n
                duxdet(icpt) = duxdet(icpt) + rl_displacement_gll_1(m_n_k)*rg_derivative_lagrange_poly(n_l)
                duydet(icpt) = duydet(icpt) + rl_displacement_gll_2(m_n_k)*rg_derivative_lagrange_poly(n_l)
                duzdet(icpt) = duzdet(icpt) + rl_displacement_gll_3(m_n_k)*rg_derivative_lagrange_poly(n_l)

                m_l_n = (n-1)*(ig_ngll*ig_ngll) + (l-1)*ig_ngll + m
                n_k = (k-1) *ig_ngll + n
                duxdze(icpt) = duxdze(icpt) + rl_displacement_gll_1(m_l_n)*rg_derivative_lagrange_poly(n_k) 
                duydze(icpt) = duydze(icpt) + rl_displacement_gll_2(m_l_n)*rg_derivative_lagrange_poly(n_k) 
                duzdze(icpt) = duzdze(icpt) + rl_displacement_gll_3(m_l_n)*rg_derivative_lagrange_poly(n_k)
             enddo

          enddo
       enddo
    enddo

    !      
    !---------->derivative of displacement at step n+1 with respect to global coordinate x, y and z at the gll node klm
    !DEC$ SIMD VECREMAINDER
    do m = 1,n3

       dxidx = rg_derivative_xi_x_of_hexa  (m)
       dxidy = rg_derivative_xi_y_of_hexa  (m)
       dxidz = rg_derivative_xi_z_of_hexa  (m)
       detdx = rg_derivative_eta_x_of_hexa (m)
       detdy = rg_derivative_eta_y_of_hexa (m)
       detdz = rg_derivative_eta_z_of_hexa (m)
       dzedx = rg_derivative_zeta_x_of_hexa(m)
       dzedy = rg_derivative_zeta_y_of_hexa(m)
       dzedz = rg_derivative_zeta_z_of_hexa(m)

       duxdx = duxdxi(m)*dxidx + duxdet(m)*detdx + duxdze(m)*dzedx
       duxdy = duxdxi(m)*dxidy + duxdet(m)*detdy + duxdze(m)*dzedy
       duxdz = duxdxi(m)*dxidz + duxdet(m)*detdz + duxdze(m)*dzedz

       duydx = duydxi(m)*dxidx + duydet(m)*detdx + duydze(m)*dzedx
       duydy = duydxi(m)*dxidy + duydet(m)*detdy + duydze(m)*dzedy
       duydz = duydxi(m)*dxidz + duydet(m)*detdz + duydze(m)*dzedz

       duzdx = duzdxi(m)*dxidx + duzdet(m)*detdx + duzdze(m)*dzedx
       duzdy = duzdxi(m)*dxidy + duzdet(m)*detdy + duzdze(m)*dzedy
       duzdz = duzdxi(m)*dxidz + duzdet(m)*detdz + duzdze(m)*dzedz

       !
       !---------->compute elastic stress (elastic simulation) or unrelaxed elastic stress (viscoelastic simulation)

       trace_tau = (rg_rhovp2_gll(m) - 2.0*rg_rhovs2_gll(m))*(duxdx+duydy+duzdz)

       tauxx     = trace_tau + 2.0*rg_rhovs2_gll(m)*duxdx
       tauyy     = trace_tau + 2.0*rg_rhovs2_gll(m)*duydy
       tauzz     = trace_tau + 2.0*rg_rhovs2_gll(m)*duzdz
       tauxy     =                 rg_rhovs2_gll(m)*(duxdy+duydx)
       tauxz     =                 rg_rhovs2_gll(m)*(duxdz+duzdx)
       tauyz     =                 rg_rhovs2_gll(m)*(duydz+duzdy)
       !
       !---------->compute viscoelastic stress
       !---------->compute viscoelastic stress

       ! remove for now

       !---------->store members of integration of the gll node klm

       intpx1(m) = rg_jacobian_determinant_of_hexa(m)*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz)
       intpx2(m) = rg_jacobian_determinant_of_hexa(m)*(tauxx*detdx+tauxy*detdy+tauxz*detdz)
       intpx3(m) = rg_jacobian_determinant_of_hexa(m)*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz)

       intpy1(m) = rg_jacobian_determinant_of_hexa(m)*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz)
       intpy2(m) = rg_jacobian_determinant_of_hexa(m)*(tauxy*detdx+tauyy*detdy+tauyz*detdz)
       intpy3(m) = rg_jacobian_determinant_of_hexa(m)*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz)

       intpz1(m) = rg_jacobian_determinant_of_hexa(m)*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz)
       intpz2(m) = rg_jacobian_determinant_of_hexa(m)*(tauxz*detdx+tauyz*detdy+tauzz*detdz)
       intpz3(m) = rg_jacobian_determinant_of_hexa(m)*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz)

    enddo !m

    !
    !->finish integration for hexa (internal forces at step n+1)
    icpt=0
    do k = 1,IG_NGLL
       do l = 1,IG_NGLL
          do m = 1,IG_NGLL

             icpt = icpt + 1

             tmpx1 = 0.0
             tmpx2 = 0.0
             tmpx3 = 0.0
             tmpy1 = 0.0
             tmpy2 = 0.0
             tmpy3 = 0.0
             tmpz1 = 0.0
             tmpz2 = 0.0
             tmpz3 = 0.0

             !DEC$ SIMD
             !DEC$ VECTOR NONTEMPORAL (n_l_k,m_n)
             do n = 1,IG_NGLL
                n_l_k = (k-1)*(ig_ngll*ig_ngll) + (l-1)*ig_ngll + n
                m_n = (n-1) *ig_ngll + m

                tmpx1 = tmpx1 + intpx1(n_l_k)*prod1(m_n,n)
                tmpy1 = tmpy1 + intpy1(n_l_k)*prod1(m_n,n)
                tmpz1 = tmpz1 + intpz1(n_l_k)*prod1(m_n,n)
             enddo

             !DEC$ SIMD
             !DEC$ VECTOR NONTEMPORAL (m_n_k,l_n)
             do n = 1,IG_NGLL
                m_n_k = (k-1)*(ig_ngll*ig_ngll) + (n-1)*ig_ngll + m
                l_n = (n-1) *ig_ngll + l
                tmpx2 = tmpx2 + intpx2(m_n_k)*prod2(l_n,n)
                tmpy2 = tmpy2 + intpy2(m_n_k)*prod2(l_n,n)
                tmpz2 = tmpz2 + intpz2(m_n_k)*prod2(l_n,n)
             enddo

             !DEC$ SIMD
             !DEC$ VECTOR NONTEMPORAL (m_l_n,k_n)
             do n = 1,IG_NGLL
                m_l_n = (n-1)*(ig_ngll*ig_ngll) + (l-1)*ig_ngll + m
                k_n = (n-1) *ig_ngll + k
                tmpx3 = tmpx3 + intpx3(m_l_n)*prod3(k_n,n)
                tmpy3 = tmpy3 + intpy3(m_l_n)*prod3(k_n,n)
                tmpz3 = tmpz3 + intpz3(m_l_n)*prod3(k_n,n)
             enddo

             fac1 = rg_weight_gll(l)*rg_weight_gll(k)
             fac2 = rg_weight_gll(m)*rg_weight_gll(k)
             fac3 = rg_weight_gll(m)*rg_weight_gll(l)

             igll = ig_global_gll_of_hexa(icpt)
             rl_acceleration_gll_1(icpt) = (fac1 *tmpx1  + fac2 *tmpx2  + fac3 *tmpx3 )
             rl_acceleration_gll_2(icpt) = (fac1 *tmpy1  + fac2 *tmpy2  + fac3 *tmpy3 )
             rl_acceleration_gll_3(icpt) = (fac1 *tmpz1  + fac2 *tmpz2  + fac3 *tmpz3 )

          enddo
       enddo
    enddo

    !DEC$ SIMD VECREMAINDER
    !DEC$ VECTOR NONTEMPORAL (igll)
    do m = 1,n3
       igll = ig_global_gll_of_hexa(m)
       rg_acceleration_gll_1(igll) = rg_acceleration_gll_1(igll) - rl_acceleration_gll_1(m)
    enddo

    !DEC$ SIMD VECREMAINDER
    !DEC$ VECTOR NONTEMPORAL (igll)
    do m = 1,n3
       igll = ig_global_gll_of_hexa(m)
       rg_acceleration_gll_2(igll) = rg_acceleration_gll_2(igll) - rl_acceleration_gll_2(m)
    enddo

    !DEC$ SIMD VECREMAINDER
    !DEC$ VECTOR NONTEMPORAL (igll)
    do m = 1,n3
       igll = ig_global_gll_of_hexa(m)
       rg_acceleration_gll_3(igll) = rg_acceleration_gll_3(igll) - rl_acceleration_gll_3(m)
    enddo


    return
    !******************************************************
  end subroutine compute_internal_forces_order4
     !******************************************************


     !
   end module mod_solver
