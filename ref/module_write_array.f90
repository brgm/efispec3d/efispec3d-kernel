!=====================================================================================================================================
!                                      EFISPEC3D                                             !
!                            (Elements FInis SPECtraux 3D)                                   !
!                                                                                            !
!                               http://efispec.free.fr                                       !
!                                                                                            !
!                                                                                            !
!                            This file is part of EFISPEC3D                                  !
!              Please refer to http://efispec.free.fr if you use it or part of it            !
!                                                                                            !
!                                                                                            !
!1 ---> French License: CeCILL V2                                                            !
!                                                                                            !
!         Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                            !
!                                              David    MICHEA                               !
!                                              Philippe THIERRY                              !
!                                                                                            !
!         Contact: f.demartin at brgm.fr                                                     !
!                                                                                            !
!         Ce logiciel est un programme informatique servant a resoudre l'equation du         !
!         mouvement en trois dimensions via une methode des elements finis spectraux.        !
!                                                                                            !
!         Ce logiciel est regi par la licence CeCILL soumise au droit francais et            !
!         respectant les principes de diffusion des logiciels libres. Vous pouvez            !
!         utiliser, modifier et/ou redistribuer ce programme sous les conditions de la       !
!         licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site       !
!         "http://www.cecill.info".                                                          !
!                                                                                            !
!         En contrepartie de l'accessibilite au code source et des droits de copie, de       !
!         modification et de redistribution accordes par cette licence, il n'est offert      !
!         aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une        !
!         responsabilite restreinte pese sur l'auteur du programme, le titulaire des         !
!         droits patrimoniaux et les concedants successifs.                                  !
!                                                                                            !
!         A cet egard l'attention de l'utilisateur est attiree sur les risques associes      !
!         au chargement, a l'utilisation, a la modification et/ou au developpement et a      !
!         la reproduction du logiciel par l'utilisateur etant donne sa specificite de        !
!         logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc     !
!         a des developpeurs et des professionnels avertis possedant des connaissances       !
!         informatiques approfondies. Les utilisateurs sont donc invites a charger et        !
!         tester l'adequation du logiciel a leurs besoins dans des conditions permettant     !
!         d'assurer la securite de leurs systemes et ou de leurs donnees et, plus            !
!         generalement, a l'utiliser et l'exploiter dans les memes conditions de             !
!         securite.                                                                          !
!                                                                                            !
!         Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris        !
!         connaissance de la licence CeCILL et que vous en avez accepte les termes.          !
!                                                                                            !
!                                                                                            !
!2 ---> International license: GNU GPL V3                                                    !
!                                                                                            !
!         EFISPEC3D is a computer program that solves the three-dimensional equations of     !
!         motion using a finite spectral-element method.                                     !
!                                                                                            !
!         Copyright (C) 2009 Florent DE MARTIN                                               !
!                                                                                            !
!         Contact: f.demartin at brgm.fr                                                     !
!                                                                                            !
!         This program is free software: you can redistribute it and/or modify it under      !
!         the terms of the GNU General Public License as published by the Free Software      !
!         Foundation, either version 3 of the License, or (at your option) any later         !
!         version.                                                                           !
!                                                                                            !
!         This program is distributed in the hope that it will be useful, but WITHOUT ANY    !
!         WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A    !
!         PARTICULAR PURPOSE. See the GNU General Public License for more details.           !
!                                                                                            !
!         You should have received a copy of the GNU General Public License along with       !
!         this program. If not, see http://www.gnu.org/licenses/.                            !
!                                                                                            !
!                                                                                            !
!3 ---> Third party libraries                                                                !
!                                                                                            !
!         EFISPEC3D uses the following of third party libraries:                             !
!                                                                                            !
!           --> METIS 5.1.0                                                                  ! 
!               see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                    !
!                                                                                            !
!           --> Lib_VTK_IO                                                                   !
!               see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                 !
!                                                                                            !
!           --> INTERP_LINEAR                                                                !
!               see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                !
!                                                                                            !
!           --> SAC                                                                          !
!               http://ds.iris.edu/files/sac-manual/                                         !
!                                                                                            !
!           --> EXODUS II                                                                    !
!               http://sourceforge.net/projects/exodusii/                                    !
!                                                                                            !
!           --> NETCDF                                                                       !
!               http://www.unidata.ucar.edu/software/netcdf/                                 !
!                                                                                            !
!           --> HDF5                                                                         !
!               http://www.hdfgroup.org/HDF5/                                                !
!                                                                                            !
!       Some of these libraries are located in directory lib and pre-compiled                !
!       with intel compiler for x86_64 platform.                                             !
!                                                                                            !
!                                                                                            !
!4 ---> Related Articles                                                                     !
!                                                                                            !
!         De Martin, F., Matsushima, M., Kawase, H. (BSSA, 2013)                             !
!            Impact of geometric effects on near-surface Green's functions                   !
!            doi:10.1785/0120130039                                                          !
!                                                                                            !
!         Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F.,      !
!         Yoshimi, M., (Pure Appl. Geophys. 2013)                                            !
!            Finite Difference Simulations of Seismic Wave Propagation for the 2007 Mw 6.6   !
!            Niigata-ken Chuetsu-Oki Earthquake: Validity of Models and Reliable             !
!            Input Ground Motion in the Near-Field                                           !
!            doi:10.1007/s00024-011-0429-5                                                   !
!                                                                                            !
!         De Martin, F. (BSSA, 2011)                                                         !
!            Verification of a Spectral-Element Method Code for the Southern California      !
!            Earthquake Center LOH.3 Viscoelastic Case                                       !
!            doi:10.1785/0120100305                                                          !
!                                                                                            !
!=====================================================================================================================================

!>@file
!!This file contains a module to write arrays to disk.

!>@brief
!!This module contains functions to write arrays to disk in unformatted form and stream access.
module mod_write_array

   use mod_global_variables, only : get_newunit
   
   implicit none

   private

   public :: write_array

!>@brief
!!Interface write_array_real to redirect to n-rank arrays.
   interface write_array

      module procedure write_array_rank1_real
      module procedure write_array_rank2_real
      module procedure write_array_rank3_real
      module procedure write_array_rank4_real
      module procedure write_array_rank5_real

      module procedure write_array_rank1_int
      module procedure write_array_rank2_int
      module procedure write_array_rank3_int
      module procedure write_array_rank4_int

   end interface write_array

   contains


!
!
!>@brief This subroutine writes to disk real value array of rank 1
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank1_real(t,tname) result(err)

      implicit none

      real            , intent(in), dimension(:) :: t
      character(len=*), intent(in)               :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 1,size(t,1),t(:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank1_real
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk real value array of rank 2
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank2_real(t,tname) result(err)

      implicit none

      real            , intent(in), dimension(:,:) :: t
      character(len=*), intent(in)                 :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 2,size(t,1),size(t,2),t(:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank2_real
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk real value array of rank 3
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank3_real(t,tname) result(err)

      implicit none

      real            , intent(in), dimension(:,:,:) :: t
      character(len=*), intent(in)                   :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)
      print *,size(t,3)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 3,size(t,1),size(t,2),size(t,3),t(:,:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank3_real
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk real value array of rank 4
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank4_real(t,tname) result(err)

      implicit none

      real            , intent(in), dimension(:,:,:,:) :: t
      character(len=*), intent(in)                     :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)
      print *,size(t,3)
      print *,size(t,4)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 4,size(t,1),size(t,2),size(t,3),size(t,4),t(:,:,:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank4_real
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk real value array of rank 5
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank5_real(t,tname) result(err)

      implicit none

      real            , intent(in), dimension(:,:,:,:,:) :: t
      character(len=*), intent(in)                       :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)
      print *,size(t,3)
      print *,size(t,4)
      print *,size(t,5)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 5,size(t,1),size(t,2),size(t,3),size(t,4),size(t,5),t(:,:,:,:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank5_real
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine writes to disk integer value array of rank 1
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank1_int(t,tname) result(err)

      implicit none

      integer         , intent(in), dimension(:) :: t
      character(len=*), intent(in)               :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 1,size(t,1),t(:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank1_int
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk integer value array of rank 2
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank2_int(t,tname) result(err)

      implicit none

      integer         , intent(in), dimension(:,:) :: t
      character(len=*), intent(in)                 :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 2,size(t,1),size(t,2),t(:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank2_int
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk integer value array of rank 3
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank3_int(t,tname) result(err)

      implicit none

      integer         , intent(in), dimension(:,:,:) :: t
      character(len=*), intent(in)                   :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)
      print *,size(t,3)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 4,size(t,1),size(t,2),size(t,3),t(:,:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank3_int
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk integer value array of rank 4
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank4_int(t,tname) result(err)

      implicit none

      integer         , intent(in), dimension(:,:,:,:) :: t
      character(len=*), intent(in)                     :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)
      print *,size(t,3)
      print *,size(t,4)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 4,size(t,1),size(t,2),size(t,3),size(t,4),t(:,:,:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank4_int
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine writes to disk integer value array of rank 5
!>@param t     : array to be written
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function write_array_rank5_int(t,tname) result(err)

      implicit none

      integer         , intent(in), dimension(:,:,:,:,:) :: t
      character(len=*), intent(in)                       :: tname

      integer :: err
      integer :: myunit

      print *,tname
      print *,size(t,1)
      print *,size(t,2)
      print *,size(t,3)
      print *,size(t,4)
      print *,size(t,5)

      open(unit=get_newunit(myunit),file=trim(tname)//".dat",action='write',access='stream',form='unformatted',iostat=err)

      write(myunit) 5,size(t,1),size(t,2),size(t,3),size(t,4),size(t,5),t(:,:,:,:,:)

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end function write_array_rank5_int
!***********************************************************************************************************************************************************************************

end module mod_write_array
