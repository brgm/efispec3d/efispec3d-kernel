
module mod_solver
!
use mpi
implicit none
!
contains
!
!***************************************************
!subroutine newmark_ini
!subroutine newmark_end
!subroutine compute_internal_forces_order4
!subroutine compute_internal_forces_order5
!subroutine compute_internal_forces_order6
!subroutine compute_absorption_forces
!***************************************************
!
!
!
!*********************************************
   subroutine newmark_ini()
!*********************************************
      use mpi
      use mod_global_variables, only :&
                                      ig_ngll_cpu&
                                     ,rg_displacement_gll&
                                     ,rg_velocity_gll&
                                     ,rg_acceleration_gll&
                                     ,rg_time_step,rg_time_step_2&
                                     ,rg_newmark_gamma

      implicit none

      integer :: igll

      do igll = 1,ig_ngll_cpu
         rg_displacement_gll(igll,1) = rg_displacement_gll(igll,1) + rg_time_step*rg_velocity_gll(igll,1) + rg_time_step_2*(rg_newmark_gamma)*rg_acceleration_gll(igll,1) !displacement x
         rg_displacement_gll(igll,2) = rg_displacement_gll(igll,2) + rg_time_step*rg_velocity_gll(igll,2) + rg_time_step_2*(rg_newmark_gamma)*rg_acceleration_gll(igll,2) !dispalcement y
         rg_displacement_gll(igll,3) = rg_displacement_gll(igll,3) + rg_time_step*rg_velocity_gll(igll,3) + rg_time_step_2*(rg_newmark_gamma)*rg_acceleration_gll(igll,3) !displacement z
      enddo

      do igll = 1,ig_ngll_cpu
         rg_acceleration_gll(igll,1) = 0.0 !flush to zero acceleration x
         rg_acceleration_gll(igll,2) = 0.0 !flush to zero acceleration y
         rg_acceleration_gll(igll,3) = 0.0 !flush to zero acceleration z
      enddo

      return
!*********************************************
   end subroutine newmark_ini
!*********************************************
!
!
!*********************************************
   subroutine newmark_end()
!*********************************************
      use mpi
      use mod_global_variables, only :&
                                      ig_ngll_cpu&
                                     ,rg_velocity_gll&
                                     ,rg_acceleration_gll&
                                     ,rg_acctmp_gll&
                                     ,rg_time_step&
                                     ,rg_newmark_gamma&
                                     ,rg_mass_matrix_gll
      implicit none

      integer :: igll

      do igll = 1,ig_ngll_cpu
         rg_acceleration_gll(igll,1) = rg_acceleration_gll(igll,1)*rg_mass_matrix_gll(igll) !acceleration x step n+1
         rg_acceleration_gll(igll,2) = rg_acceleration_gll(igll,2)*rg_mass_matrix_gll(igll) !acceleration y step n+1
         rg_acceleration_gll(igll,3) = rg_acceleration_gll(igll,3)*rg_mass_matrix_gll(igll) !acceleration z step n+1
      enddo
 
      do igll = 1,ig_ngll_cpu
         rg_velocity_gll(igll,1) = rg_velocity_gll(igll,1) + rg_time_step*((1.0e0-rg_newmark_gamma)*rg_acctmp_gll(igll,1) + rg_newmark_gamma*rg_acceleration_gll(igll,1)) !velocity x step n+1
         rg_velocity_gll(igll,2) = rg_velocity_gll(igll,2) + rg_time_step*((1.0e0-rg_newmark_gamma)*rg_acctmp_gll(igll,2) + rg_newmark_gamma*rg_acceleration_gll(igll,2)) !velocity y step n+1
         rg_velocity_gll(igll,3) = rg_velocity_gll(igll,3) + rg_time_step*((1.0e0-rg_newmark_gamma)*rg_acctmp_gll(igll,3) + rg_newmark_gamma*rg_acceleration_gll(igll,3)) !velocity z step n+1
      enddo
 
      do igll = 1,ig_ngll_cpu
         rg_acctmp_gll(igll,1)   = rg_acceleration_gll(igll,1) !store tmp acceleration for next step
         rg_acctmp_gll(igll,2)   = rg_acceleration_gll(igll,2) !store tmp acceleration for next step
         rg_acctmp_gll(igll,3)   = rg_acceleration_gll(igll,3) !store tmp acceleration for next step
      enddo

      return
!*********************************************
   end subroutine newmark_end
!*********************************************

!
!
!*****************************************************************
   subroutine compute_internal_forces_order4(elt_start,elt_end)
!*****************************************************************
      use mpi
      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,rg_derivative_lagrange_poly&
                                     ,rg_displacement_gll&
                                     ,rg_acceleration_gll&
                                     ,rg_weight_gll&
                                     ,rg_abscissa_gll&
                                     ,ig_number_of_receiver_cpu&
                                     ,ig_current_time_step&
                                     ,ig_freq_write_rec_time_history&
                                     ,ig_number_of_hexa_cpu&
                                     ,ig_global_gll_of_hexa&
                                     ,rg_derivative_xi_x_of_hexa&
                                     ,rg_derivative_xi_y_of_hexa&
                                     ,rg_derivative_xi_z_of_hexa&
                                     ,rg_derivative_eta_x_of_hexa&
                                     ,rg_derivative_eta_y_of_hexa&
                                     ,rg_derivative_eta_z_of_hexa&
                                     ,rg_derivative_zeta_x_of_hexa&
                                     ,rg_derivative_zeta_y_of_hexa&
                                     ,rg_derivative_zeta_z_of_hexa&
                                     ,rg_jacobian_determinant_of_hexa&
                                     ,ig_my_rank&
                                     ,ig_number_of_outer_hexa_cpu&
                                     ,rg_rho_gll&
                                     ,rg_rhovs2_gll&
                                     ,rg_rhovp2_gll&
                                     ,rg_rhovs2_gll&
                                     ,rg_rhovp2_gll&
                                     ,rg_wkqs_gll&
                                     ,rg_wkqp_gll&
                                     ,rg_memory_variables_ksixx&
                                     ,rg_memory_variables_ksiyy&
                                     ,rg_memory_variables_ksizz&
                                     ,rg_memory_variables_ksixy&
                                     ,rg_memory_variables_ksixz&
                                     ,rg_memory_variables_ksiyz&
                                     ,rg_coeff_memory_variables&
                                     ,ig_number_of_memory_var&
                                     ,rg_time_step&
                                     ,IS_VISCO&
                                     ,ig_number_of_hexa_cpu
      
      implicit none
      
      integer, intent(in)          :: elt_start,elt_end
      
      real    :: intpx1(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpx2(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpx3(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpy1(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpy2(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpy3(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpz1(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpz2(IG_NGLL,IG_NGLL,IG_NGLL)
      real    :: intpz3(IG_NGLL,IG_NGLL,IG_NGLL) 

      real   :: duxdxi,duxdet,duxdze,duydxi,duydet,duydze,duzdxi,duzdet,duzdze,duxdx,duydy,duzdz,duxdy,duxdz,duydx,duydz,duzdx,duzdy,dxidx,dxidy,dxidz,detdx,detdy,detdz,dzedx,dzedy,dzedz,tauxx,tauyy,tauzz,tauxy,tauxz,tauyz,tauxx_n12,tauyy_n12,tauzz_n12,tauxy_n12,tauxz_n12,tauyz_n12,trace_tau

      real    :: tmpx1
      real    :: tmpx2
      real    :: tmpx3
      real    :: tmpy1
      real    :: tmpy2
      real    :: tmpy3
      real    :: tmpz1
      real    :: tmpz2
      real    :: tmpz3
      real    :: fac1
      real    :: fac2
      real    :: fac3
      real    :: rl_displacement_gll(IG_NGLL,IG_NGLL,IG_NGLL,3)
      real    :: rl_acceleration_gll(IG_NGLL,IG_NGLL,IG_NGLL,3)
      
      integer :: ios
      integer :: iel
      integer :: i
      integer :: j
      integer :: k
      integer :: l
      integer :: m
      integer :: n
      integer :: igll
      integer :: imem_var


      do iel = elt_start,elt_end

       
         !
         !------->fill local displacement
!DEC$ VECTOR NONTEMPORAL (igll)
         do k = 1,IG_NGLL        !zeta
            do l = 1,IG_NGLL     !eta
             do m = 1,IG_NGLL  !xi
                  igll                         = ig_global_gll_of_hexa(m,l,k,iel)
                  rl_displacement_gll(m,l,k,1) = rg_displacement_gll(igll,1)
                  rl_displacement_gll(m,l,k,2) = rg_displacement_gll(igll,2)
                  rl_displacement_gll(m,l,k,3) = rg_displacement_gll(igll,3)
               enddo
            enddo
         enddo
      !

      !
      !******************************************************************************
      !->compute integrale at gll nodes + assemble force in global gll grid for hexa
      !******************************************************************************

         do k = 1,IG_NGLL        !zeta
            do l = 1,IG_NGLL     !eta

               do m = 1,IG_NGLL  !xi

      !---------->derivative of displacement with respect to local coordinate xi, eta and zeta at the gll node klm

                  duxdxi = rl_displacement_gll(1,l,k,1)*rg_derivative_lagrange_poly(1,m) &
                         + rl_displacement_gll(2,l,k,1)*rg_derivative_lagrange_poly(2,m) &
                         + rl_displacement_gll(3,l,k,1)*rg_derivative_lagrange_poly(3,m) &
                         + rl_displacement_gll(4,l,k,1)*rg_derivative_lagrange_poly(4,m) &
                         + rl_displacement_gll(5,l,k,1)*rg_derivative_lagrange_poly(5,m) 

                  duxdet = rl_displacement_gll(m,1,k,1)*rg_derivative_lagrange_poly(1,l) &
                         + rl_displacement_gll(m,2,k,1)*rg_derivative_lagrange_poly(2,l) &
                         + rl_displacement_gll(m,3,k,1)*rg_derivative_lagrange_poly(3,l) &
                         + rl_displacement_gll(m,4,k,1)*rg_derivative_lagrange_poly(4,l) &
                         + rl_displacement_gll(m,5,k,1)*rg_derivative_lagrange_poly(5,l)

                  duxdze = rl_displacement_gll(m,l,1,1)*rg_derivative_lagrange_poly(1,k) &
                         + rl_displacement_gll(m,l,2,1)*rg_derivative_lagrange_poly(2,k) &
                         + rl_displacement_gll(m,l,3,1)*rg_derivative_lagrange_poly(3,k) &
                         + rl_displacement_gll(m,l,4,1)*rg_derivative_lagrange_poly(4,k) &
                         + rl_displacement_gll(m,l,5,1)*rg_derivative_lagrange_poly(5,k) 

                  duydxi = rl_displacement_gll(1,l,k,2)*rg_derivative_lagrange_poly(1,m) &
                         + rl_displacement_gll(2,l,k,2)*rg_derivative_lagrange_poly(2,m) &
                         + rl_displacement_gll(3,l,k,2)*rg_derivative_lagrange_poly(3,m) &
                         + rl_displacement_gll(4,l,k,2)*rg_derivative_lagrange_poly(4,m) &
                         + rl_displacement_gll(5,l,k,2)*rg_derivative_lagrange_poly(5,m) 

                  duydet = rl_displacement_gll(m,1,k,2)*rg_derivative_lagrange_poly(1,l) &
                         + rl_displacement_gll(m,2,k,2)*rg_derivative_lagrange_poly(2,l) &
                         + rl_displacement_gll(m,3,k,2)*rg_derivative_lagrange_poly(3,l) &
                         + rl_displacement_gll(m,4,k,2)*rg_derivative_lagrange_poly(4,l) &
                         + rl_displacement_gll(m,5,k,2)*rg_derivative_lagrange_poly(5,l)

                  duydze = rl_displacement_gll(m,l,1,2)*rg_derivative_lagrange_poly(1,k) &
                         + rl_displacement_gll(m,l,2,2)*rg_derivative_lagrange_poly(2,k) &
                         + rl_displacement_gll(m,l,3,2)*rg_derivative_lagrange_poly(3,k) &
                         + rl_displacement_gll(m,l,4,2)*rg_derivative_lagrange_poly(4,k) &
                         + rl_displacement_gll(m,l,5,2)*rg_derivative_lagrange_poly(5,k) 

                  duzdxi = rl_displacement_gll(1,l,k,3)*rg_derivative_lagrange_poly(1,m) &
                         + rl_displacement_gll(2,l,k,3)*rg_derivative_lagrange_poly(2,m) &
                         + rl_displacement_gll(3,l,k,3)*rg_derivative_lagrange_poly(3,m) &
                         + rl_displacement_gll(4,l,k,3)*rg_derivative_lagrange_poly(4,m) &
                         + rl_displacement_gll(5,l,k,3)*rg_derivative_lagrange_poly(5,m)

                  duzdet = rl_displacement_gll(m,1,k,3)*rg_derivative_lagrange_poly(1,l) &
                         + rl_displacement_gll(m,2,k,3)*rg_derivative_lagrange_poly(2,l) &
                         + rl_displacement_gll(m,3,k,3)*rg_derivative_lagrange_poly(3,l) &
                         + rl_displacement_gll(m,4,k,3)*rg_derivative_lagrange_poly(4,l) &
                         + rl_displacement_gll(m,5,k,3)*rg_derivative_lagrange_poly(5,l)

                  duzdze = rl_displacement_gll(m,l,1,3)*rg_derivative_lagrange_poly(1,k) &
                         + rl_displacement_gll(m,l,2,3)*rg_derivative_lagrange_poly(2,k) &
                         + rl_displacement_gll(m,l,3,3)*rg_derivative_lagrange_poly(3,k) &
                         + rl_displacement_gll(m,l,4,3)*rg_derivative_lagrange_poly(4,k) &
                         + rl_displacement_gll(m,l,5,3)*rg_derivative_lagrange_poly(5,k)


      !      
      !---------->derivative of displacement at step n+1 with respect to global coordinate x, y and z at the gll node klm
                  dxidx = rg_derivative_xi_x_of_hexa  (m,l,k,iel)
                  dxidy = rg_derivative_xi_y_of_hexa  (m,l,k,iel)
                  dxidz = rg_derivative_xi_z_of_hexa  (m,l,k,iel)

                  detdx = rg_derivative_eta_x_of_hexa (m,l,k,iel)
                  detdy = rg_derivative_eta_y_of_hexa (m,l,k,iel)
                  detdz = rg_derivative_eta_z_of_hexa (m,l,k,iel)

                  dzedx = rg_derivative_zeta_x_of_hexa(m,l,k,iel)
                  dzedy = rg_derivative_zeta_y_of_hexa(m,l,k,iel)
                  dzedz = rg_derivative_zeta_z_of_hexa(m,l,k,iel)

                  duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx
                  duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy
                  duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz
                  duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx
                  duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy
                  duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz
                  duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx
                  duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy
                  duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz
      !
      !---------->compute elastic stress (elastic simulation) or unrelaxed elastic stress (viscoelastic simulation)
                  trace_tau = (rg_rhovp2_gll(m,l,k,iel) - 2.0*rg_rhovs2_gll(m,l,k,iel))*(duxdx+duydy+duzdz)
                  tauxx     = trace_tau + 2.0*rg_rhovs2_gll(m,l,k,iel)*duxdx
                  tauyy     = trace_tau + 2.0*rg_rhovs2_gll(m,l,k,iel)*duydy
                  tauzz     = trace_tau + 2.0*rg_rhovs2_gll(m,l,k,iel)*duzdz
                  tauxy     =                 rg_rhovs2_gll(m,l,k,iel)*(duxdy+duydx)
                  tauxz     =                 rg_rhovs2_gll(m,l,k,iel)*(duxdz+duzdx)
                  tauyz     =                 rg_rhovs2_gll(m,l,k,iel)*(duydz+duzdy)
      !
      !---------->compute viscoelastic stress

      !---------->store members of integration of the gll node klm
                  intpx1(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz)
                  intpx2(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxx*detdx+tauxy*detdy+tauxz*detdz)
                  intpx3(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz)

                  intpy1(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz)
                  intpy2(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxy*detdx+tauyy*detdy+tauyz*detdz)
                  intpy3(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz)

                  intpz1(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz)
                  intpz2(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxz*detdx+tauyz*detdy+tauzz*detdz)
                  intpz3(m,l,k) = rg_jacobian_determinant_of_hexa(m,l,k,iel)*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz)

               enddo !xi
            enddo    !eta
         enddo       !zeta

      !
      !->finish integration for hexa (internal forces at step n+1)
!DEC$ VECTOR NONTEMPORAL (tmpx1,tmpx2,tmpx3,tmpy1,tmpy2,tmpy3,tmpz1,tmpz2,tmpz3,fac1,fac2,fac3)
         do k = 1,IG_NGLL
            do l = 1,IG_NGLL
               do m = 1,IG_NGLL

                  tmpx1 = intpx1(1,l,k)*rg_derivative_lagrange_poly(m,1)*rg_weight_gll(1) &
                        + intpx1(2,l,k)*rg_derivative_lagrange_poly(m,2)*rg_weight_gll(2) &
                        + intpx1(3,l,k)*rg_derivative_lagrange_poly(m,3)*rg_weight_gll(3) &
                        + intpx1(4,l,k)*rg_derivative_lagrange_poly(m,4)*rg_weight_gll(4) &
                        + intpx1(5,l,k)*rg_derivative_lagrange_poly(m,5)*rg_weight_gll(5)

                  tmpy1 = intpy1(1,l,k)*rg_derivative_lagrange_poly(m,1)*rg_weight_gll(1) &
                        + intpy1(2,l,k)*rg_derivative_lagrange_poly(m,2)*rg_weight_gll(2) &
                        + intpy1(3,l,k)*rg_derivative_lagrange_poly(m,3)*rg_weight_gll(3) &
                        + intpy1(4,l,k)*rg_derivative_lagrange_poly(m,4)*rg_weight_gll(4) &
                        + intpy1(5,l,k)*rg_derivative_lagrange_poly(m,5)*rg_weight_gll(5)

                  tmpz1 = intpz1(1,l,k)*rg_derivative_lagrange_poly(m,1)*rg_weight_gll(1) &
                        + intpz1(2,l,k)*rg_derivative_lagrange_poly(m,2)*rg_weight_gll(2) &
                        + intpz1(3,l,k)*rg_derivative_lagrange_poly(m,3)*rg_weight_gll(3) &
                        + intpz1(4,l,k)*rg_derivative_lagrange_poly(m,4)*rg_weight_gll(4) &
                        + intpz1(5,l,k)*rg_derivative_lagrange_poly(m,5)*rg_weight_gll(5)

                  tmpx2 = intpx2(m,1,k)*rg_derivative_lagrange_poly(l,1)*rg_weight_gll(1) &
                        + intpx2(m,2,k)*rg_derivative_lagrange_poly(l,2)*rg_weight_gll(2) &
                        + intpx2(m,3,k)*rg_derivative_lagrange_poly(l,3)*rg_weight_gll(3) &
                        + intpx2(m,4,k)*rg_derivative_lagrange_poly(l,4)*rg_weight_gll(4) &
                        + intpx2(m,5,k)*rg_derivative_lagrange_poly(l,5)*rg_weight_gll(5)

                  tmpy2 = intpy2(m,1,k)*rg_derivative_lagrange_poly(l,1)*rg_weight_gll(1) &
                        + intpy2(m,2,k)*rg_derivative_lagrange_poly(l,2)*rg_weight_gll(2) &
                        + intpy2(m,3,k)*rg_derivative_lagrange_poly(l,3)*rg_weight_gll(3) &
                        + intpy2(m,4,k)*rg_derivative_lagrange_poly(l,4)*rg_weight_gll(4) &
                        + intpy2(m,5,k)*rg_derivative_lagrange_poly(l,5)*rg_weight_gll(5)

                  tmpz2 = intpz2(m,1,k)*rg_derivative_lagrange_poly(l,1)*rg_weight_gll(1) &
                        + intpz2(m,2,k)*rg_derivative_lagrange_poly(l,2)*rg_weight_gll(2) &
                        + intpz2(m,3,k)*rg_derivative_lagrange_poly(l,3)*rg_weight_gll(3) &
                        + intpz2(m,4,k)*rg_derivative_lagrange_poly(l,4)*rg_weight_gll(4) &
                        + intpz2(m,5,k)*rg_derivative_lagrange_poly(l,5)*rg_weight_gll(5)

                  tmpx3 = intpx3(m,l,1)*rg_derivative_lagrange_poly(k,1)*rg_weight_gll(1) &
                        + intpx3(m,l,2)*rg_derivative_lagrange_poly(k,2)*rg_weight_gll(2) &
                        + intpx3(m,l,3)*rg_derivative_lagrange_poly(k,3)*rg_weight_gll(3) &
                        + intpx3(m,l,4)*rg_derivative_lagrange_poly(k,4)*rg_weight_gll(4) &
                        + intpx3(m,l,5)*rg_derivative_lagrange_poly(k,5)*rg_weight_gll(5)

                  tmpy3 = intpy3(m,l,1)*rg_derivative_lagrange_poly(k,1)*rg_weight_gll(1) &
                        + intpy3(m,l,2)*rg_derivative_lagrange_poly(k,2)*rg_weight_gll(2) &
                        + intpy3(m,l,3)*rg_derivative_lagrange_poly(k,3)*rg_weight_gll(3) &
                        + intpy3(m,l,4)*rg_derivative_lagrange_poly(k,4)*rg_weight_gll(4) &
                        + intpy3(m,l,5)*rg_derivative_lagrange_poly(k,5)*rg_weight_gll(5)

                  tmpz3 = intpz3(m,l,1)*rg_derivative_lagrange_poly(k,1)*rg_weight_gll(1) &
                        + intpz3(m,l,2)*rg_derivative_lagrange_poly(k,2)*rg_weight_gll(2) &
                        + intpz3(m,l,3)*rg_derivative_lagrange_poly(k,3)*rg_weight_gll(3) &
                        + intpz3(m,l,4)*rg_derivative_lagrange_poly(k,4)*rg_weight_gll(4) &
                        + intpz3(m,l,5)*rg_derivative_lagrange_poly(k,5)*rg_weight_gll(5) 

                  fac1 = rg_weight_gll(l)*rg_weight_gll(k)
                  fac2 = rg_weight_gll(m)*rg_weight_gll(k)
                  fac3 = rg_weight_gll(m)*rg_weight_gll(l)

                  rl_acceleration_gll(m,l,k,1) = (fac1*tmpx1 + fac2*tmpx2 + fac3*tmpx3)
                  rl_acceleration_gll(m,l,k,2) = (fac1*tmpy1 + fac2*tmpy2 + fac3*tmpy3)
                  rl_acceleration_gll(m,l,k,3) = (fac1*tmpz1 + fac2*tmpz2 + fac3*tmpz3)

               enddo
            enddo
         enddo

         do k = 1,IG_NGLL        !zeta
            do l = 1,IG_NGLL     !eta
               do m = 1,IG_NGLL  !xi

                  igll                        = ig_global_gll_of_hexa(m,l,k,iel)

                  rg_acceleration_gll(igll,1) = rg_acceleration_gll(igll,1) - rl_acceleration_gll(m,l,k,1)
                  rg_acceleration_gll(igll,2) = rg_acceleration_gll(igll,2) - rl_acceleration_gll(m,l,k,2)
                  rg_acceleration_gll(igll,3) = rg_acceleration_gll(igll,3) - rl_acceleration_gll(m,l,k,3)

               enddo
            enddo
         enddo

      enddo !loop on element

      return
!******************************************************
   end subroutine compute_internal_forces_order4
!******************************************************


!
!
!*********************************************************
   subroutine compute_absorption_forces()
!*********************************************************
   use mpi
   use mod_global_variables, only : &
                                    rg_velocity_gll&
                                   ,rg_acceleration_gll&
                                   ,rg_acctmp_gll&
                                   ,rg_time_step&
                                   ,rg_newmark_gamma&
                                   ,IG_NGLL&
                                   ,rg_weight_gll&
                                   ,ig_number_of_quad_parax_cpu&
                                   ,rg_jacobian_determinant_of_quad&
                                   ,rg_normal_to_quad&
                                   ,ig_global_gll_of_quad_parax&
                                   ,rg_rhovp_gll_parax&
                                   ,rg_rhovs_gll_parax
   !
   implicit none
   !
   real          :: vx
   real          :: vy
   real          :: vz
   real          :: jaco
   real          :: nx
   real          :: ny
   real          :: nz
   real          :: tx
   real          :: ty
   real          :: tz
   real          :: vn
   !
   integer       :: j
   integer       :: k
   integer       :: l
   integer       :: igll
   !
   !
   !****************************************************************
   !->compute integrale at gll nodes for explicit paraxial element
   !->paraxial order 0: p1 of clayton or stacey
   !****************************************************************
   do j = 1,ig_number_of_quad_parax_cpu
      do k = 1,IG_NGLL
        do l = 1,IG_NGLL
            jaco   =  rg_jacobian_determinant_of_quad(l,k,j)
            nx     =  rg_normal_to_quad(l,k,j,1)
            ny     =  rg_normal_to_quad(l,k,j,2)
            nz     =  rg_normal_to_quad(l,k,j,3)
 
            igll   = ig_global_gll_of_quad_parax(l,k,j)
   
            vx     = rg_velocity_gll(igll,1) + rg_time_step*(rg_newmark_gamma*rg_acctmp_gll(igll,1)) 
            vy     = rg_velocity_gll(igll,2) + rg_time_step*(rg_newmark_gamma*rg_acctmp_gll(igll,2)) 
            vz     = rg_velocity_gll(igll,3) + rg_time_step*(rg_newmark_gamma*rg_acctmp_gll(igll,3)) 

            vn     = vx*nx+vy*ny+vz*nz
 
            tx     =  rg_rhovp_gll_parax(l,k,j)*vn*nx + rg_rhovs_gll_parax(l,k,j)*(vx-vn*nx)
            ty     =  rg_rhovp_gll_parax(l,k,j)*vn*ny + rg_rhovs_gll_parax(l,k,j)*(vy-vn*ny)
            tz     =  rg_rhovp_gll_parax(l,k,j)*vn*nz + rg_rhovs_gll_parax(l,k,j)*(vz-vn*nz)
    
            rg_acceleration_gll(igll,1) = rg_acceleration_gll(igll,1) - jaco*rg_weight_gll(l)*rg_weight_gll(k)*tx
            rg_acceleration_gll(igll,2) = rg_acceleration_gll(igll,2) - jaco*rg_weight_gll(l)*rg_weight_gll(k)*ty
            rg_acceleration_gll(igll,3) = rg_acceleration_gll(igll,3) - jaco*rg_weight_gll(l)*rg_weight_gll(k)*tz

         enddo
      enddo
   enddo !loop on quad element (paraxial)

   return
!*********************************************************
   end subroutine compute_absorption_forces
!*********************************************************
!
end module mod_solver
