module mod_solver
  !
  use mpi
  implicit none
  !
contains
  !
  !***************************************************
  !subroutine compute_internal_forces_order4
  !subroutine compute_internal_forces_order5
  !subroutine compute_internal_forces_order6
  !***************************************************
  !
  !*****************************************************************
  subroutine compute_internal_forces_order4(&
       ig_global_gll_of_hexa,&
       rg_derivative_xi_x_of_hexa  ,&
       rg_derivative_xi_y_of_hexa  ,&
       rg_derivative_xi_z_of_hexa  ,&
       rg_derivative_eta_x_of_hexa ,&
       rg_derivative_eta_y_of_hexa ,&
       rg_derivative_eta_z_of_hexa ,&
       rg_derivative_zeta_x_of_hexa,&
       rg_derivative_zeta_y_of_hexa,&
       rg_derivative_zeta_z_of_hexa,&
       rg_rhovp2_gll,&
       rg_rhovs2_gll,&
       rg_jacobian_determinant_of_hexa)

    !*****************************************************************
    use mpi
    use mod_global_variables, only :&
         IG_NGLL&
         ,rg_derivative_lagrange_poly&
         ,rg_displacement_gll_1,rg_displacement_gll_2,rg_displacement_gll_3&
         ,rg_acceleration_gll_1,rg_acceleration_gll_2,rg_acceleration_gll_3&
         ,rg_weight_gll&
         ,rg_abscissa_gll&
         ,ig_number_of_receiver_cpu&
         ,ig_current_time_step&
         ,ig_freq_write_rec_time_history&
         ,ig_number_of_hexa_cpu&
         ,ig_my_rank&
         ,rg_rho_gll&
         ,rg_wkqs_gll&
         ,rg_wkqp_gll&
         ,rg_memory_variables_ksixx&
         ,rg_memory_variables_ksiyy&
         ,rg_memory_variables_ksizz&
         ,rg_memory_variables_ksixy&
         ,rg_memory_variables_ksixz&
         ,rg_memory_variables_ksiyz&
         ,rg_coeff_memory_variables&
         ,ig_number_of_memory_var&
         ,rg_time_step&
         ,IS_VISCO&
         ,ig_number_of_hexa_cpu&
         ,n_l_k_1,m_n_k_1,m_l_n_1&
         ,n_m_1,m_n_1,n_l_1,l_n_1,n_k_1,k_n_1&
         ,n_l_k_2,m_n_k_2,m_l_n_2&
         ,n_m_2,m_n_2,n_l_2,l_n_2,n_k_2,k_n_2&
         ,n_l_k_3,m_n_k_3,m_l_n_3&
         ,n_m_3,m_n_3,n_l_3,l_n_3,n_k_3,k_n_3&
         ,n_l_k_4,m_n_k_4,m_l_n_4&
         ,n_m_4,m_n_4,n_l_4,l_n_4,n_k_4,k_n_4&
         ,n_l_k_5,m_n_k_5,m_l_n_5&
         ,n_m_5,m_n_5,n_l_5,l_n_5,n_k_5,k_n_5&
         !         ,n_l_k,m_n_k,m_l_n&
         !         ,n_m,m_n,n_l,l_n,n_k,k_n&
         ,fac1,fac2,fac3,prod1,prod2,prod3

    implicit none

    !     integer, intent(in)          :: elt_start,elt_end

    integer, parameter :: n3=ig_ngll*ig_ngll*ig_ngll
    integer, dimension(n3) ::          ig_global_gll_of_hexa
    real,dimension(n3) ::      &
         rg_derivative_xi_x_of_hexa  ,&
         rg_derivative_xi_y_of_hexa  ,&
         rg_derivative_xi_z_of_hexa  ,&
         rg_derivative_eta_x_of_hexa ,&
         rg_derivative_eta_y_of_hexa ,&
         rg_derivative_eta_z_of_hexa ,&
         rg_derivative_zeta_x_of_hexa,&
         rg_derivative_zeta_y_of_hexa,&
         rg_derivative_zeta_z_of_hexa,&
         rg_rhovp2_gll,&
         rg_rhovs2_gll,&
         rg_jacobian_determinant_of_hexa

    real    :: intpx1(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpx2(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpx3(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpy1(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpy2(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpy3(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpz1(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpz2(ig_ngll*ig_ngll*ig_ngll)
    real    :: intpz3(ig_ngll*ig_ngll*ig_ngll) 

!!$    real :: duxdxi,duxdet,duxdze
!!$    real :: duydxi,duydet,duydze
!!$    real :: duzdxi,duzdet,duzdze

    real,dimension(n3) :: duxdxi,duxdet,duxdze
    real,dimension(n3) :: duydxi,duydet,duydze
    real,dimension(n3) :: duzdxi,duzdet,duzdze

    real    :: tmpx1,tmpx2,tmpx3,tmpy1,tmpy2,tmpy3,tmpz1,tmpz2,tmpz3
    !,fac1,fac2,fac3

    real    :: duxdx,duxdy,duxdz
    real    :: duydx,duydy,duydz
    real    :: duzdx,duzdy,duzdz

    real    :: dxidx,dxidy,dxidz
    real    :: detdx,detdy,detdz
    real    :: dzedx,dzedy,dzedz
    real    :: tauxx,tauyy,tauzz
    real    :: tauxy,tauxz,tauyz

    real    :: tauxx_n12,tauyy_n12,tauzz_n12
    real    :: tauxy_n12,tauxz_n12,tauyz_n12
    real    :: trace_tau

!        real,dimension(n3) :: rl_acceleration_gll_1,rl_acceleration_gll_2,rl_acceleration_gll_3
!        real,dimension(n3) :: rl_displacement_gll_1,rl_displacement_gll_2,rl_displacement_gll_3

    integer :: ios,iel,igll
    integer :: i,j,k,l,m,n
    integer :: imem_var
    integer :: i3,icpt


    !
    !------->flush local acceleration to zero


    !******************************************************************************
    !->compute integrale at gll nodes + assemble force in global gll grid for hexa
!DEC$ SIMD
    do m=1,n3   
       duxdxi(m) = &
            rg_displacement_gll_1(ig_global_gll_of_hexa(n_l_k_1(m)))*rg_derivative_lagrange_poly(n_m_1(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(n_l_k_2(m)))*rg_derivative_lagrange_poly(n_m_2(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(n_l_k_3(m)))*rg_derivative_lagrange_poly(n_m_3(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(n_l_k_4(m)))*rg_derivative_lagrange_poly(n_m_4(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(n_l_k_5(m)))*rg_derivative_lagrange_poly(n_m_5(m)) 

       duxdet(m) = &
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_n_k_1(m)))*rg_derivative_lagrange_poly(n_l_1(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_n_k_2(m)))*rg_derivative_lagrange_poly(n_l_2(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_n_k_3(m)))*rg_derivative_lagrange_poly(n_l_3(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_n_k_4(m)))*rg_derivative_lagrange_poly(n_l_4(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_n_k_5(m)))*rg_derivative_lagrange_poly(n_l_5(m)) 

       duxdze(m) = &
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_l_n_1(m)))*rg_derivative_lagrange_poly(n_k_1(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_l_n_2(m)))*rg_derivative_lagrange_poly(n_k_2(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_l_n_3(m)))*rg_derivative_lagrange_poly(n_k_3(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_l_n_4(m)))*rg_derivative_lagrange_poly(n_k_4(m)) +&
            rg_displacement_gll_1(ig_global_gll_of_hexa(m_l_n_5(m)))*rg_derivative_lagrange_poly(n_k_5(m)) 

    enddo

!DEC$ SIMD
    do m=1,n3   
       duydxi(m) =  &
            rg_displacement_gll_2(ig_global_gll_of_hexa(n_l_k_1(m)))*rg_derivative_lagrange_poly(n_m_1(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(n_l_k_2(m)))*rg_derivative_lagrange_poly(n_m_2(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(n_l_k_3(m)))*rg_derivative_lagrange_poly(n_m_3(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(n_l_k_4(m)))*rg_derivative_lagrange_poly(n_m_4(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(n_l_k_5(m)))*rg_derivative_lagrange_poly(n_m_5(m)) 

       duydet(m) = &
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_n_k_1(m)))*rg_derivative_lagrange_poly(n_l_1(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_n_k_2(m)))*rg_derivative_lagrange_poly(n_l_2(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_n_k_3(m)))*rg_derivative_lagrange_poly(n_l_3(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_n_k_4(m)))*rg_derivative_lagrange_poly(n_l_4(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_n_k_5(m)))*rg_derivative_lagrange_poly(n_l_5(m)) 

       duydze(m) = &
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_l_n_1(m)))*rg_derivative_lagrange_poly(n_k_1(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_l_n_2(m)))*rg_derivative_lagrange_poly(n_k_2(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_l_n_3(m)))*rg_derivative_lagrange_poly(n_k_3(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_l_n_4(m)))*rg_derivative_lagrange_poly(n_k_4(m)) +&
            rg_displacement_gll_2(ig_global_gll_of_hexa(m_l_n_5(m)))*rg_derivative_lagrange_poly(n_k_5(m)) 
    enddo

!DEC$ SIMD
    do m=1,n3   

       duzdxi(m) = &
            rg_displacement_gll_3(ig_global_gll_of_hexa(n_l_k_1(m)))*rg_derivative_lagrange_poly(n_m_1(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(n_l_k_2(m)))*rg_derivative_lagrange_poly(n_m_2(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(n_l_k_3(m)))*rg_derivative_lagrange_poly(n_m_3(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(n_l_k_4(m)))*rg_derivative_lagrange_poly(n_m_4(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(n_l_k_5(m)))*rg_derivative_lagrange_poly(n_m_5(m)) 

       duzdet(m) = &
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_n_k_1(m)))*rg_derivative_lagrange_poly(n_l_1(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_n_k_2(m)))*rg_derivative_lagrange_poly(n_l_2(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_n_k_3(m)))*rg_derivative_lagrange_poly(n_l_3(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_n_k_4(m)))*rg_derivative_lagrange_poly(n_l_4(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_n_k_5(m)))*rg_derivative_lagrange_poly(n_l_5(m)) 

       duzdze(m) = &
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_l_n_1(m)))*rg_derivative_lagrange_poly(n_k_1(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_l_n_2(m)))*rg_derivative_lagrange_poly(n_k_2(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_l_n_3(m)))*rg_derivative_lagrange_poly(n_k_3(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_l_n_4(m)))*rg_derivative_lagrange_poly(n_k_4(m)) +&
            rg_displacement_gll_3(ig_global_gll_of_hexa(m_l_n_5(m)))*rg_derivative_lagrange_poly(n_k_5(m)) 
enddo

!DEC$ SIMD
    do m=1,n3   
!!$
       dxidx = rg_derivative_xi_x_of_hexa(m)
       dxidy = rg_derivative_xi_y_of_hexa(m)
       dxidz = rg_derivative_xi_z_of_hexa(m)

       detdx = rg_derivative_eta_x_of_hexa(m)
       detdy = rg_derivative_eta_y_of_hexa(m)
       detdz = rg_derivative_eta_z_of_hexa(m)

       dzedx = rg_derivative_zeta_x_of_hexa(m)
       dzedy = rg_derivative_zeta_y_of_hexa(m)
       dzedz = rg_derivative_zeta_z_of_hexa(m)
!!$


       duxdx = duxdxi(m)*dxidx + duxdet(m)*detdx + duxdze(m)*dzedx
       duxdy = duxdxi(m)*dxidy + duxdet(m)*detdy + duxdze(m)*dzedy
       duxdz = duxdxi(m)*dxidz + duxdet(m)*detdz + duxdze(m)*dzedz

       duydx = duydxi(m)*dxidx + duydet(m)*detdx + duydze(m)*dzedx
       duydy = duydxi(m)*dxidy + duydet(m)*detdy + duydze(m)*dzedy
       duydz = duydxi(m)*dxidz + duydet(m)*detdz + duydze(m)*dzedz

       duzdx = duzdxi(m)*dxidx + duzdet(m)*detdx + duzdze(m)*dzedx
       duzdy = duzdxi(m)*dxidy + duzdet(m)*detdy + duzdze(m)*dzedy
       duzdz = duzdxi(m)*dxidz + duzdet(m)*detdz + duzdze(m)*dzedz

!!$
!!$       duxdx = duxdxi*rg_derivative_xi_x_of_hexa(m) + duxdet*rg_derivative_eta_x_of_hexa(m) + duxdze*rg_derivative_zeta_x_of_hexa(m)
!!$       duxdy = duxdxi*rg_derivative_xi_y_of_hexa(m) + duxdet*rg_derivative_eta_y_of_hexa(m) + duxdze*rg_derivative_zeta_y_of_hexa(m)
!!$       duxdz = duxdxi*rg_derivative_xi_z_of_hexa(m) + duxdet*rg_derivative_eta_z_of_hexa(m) + duxdze*rg_derivative_zeta_z_of_hexa(m)
!!$
!!$       duydx = duydxi*rg_derivative_xi_x_of_hexa(m) + duydet*rg_derivative_eta_x_of_hexa(m) + duydze*rg_derivative_zeta_x_of_hexa(m)
!!$       duydy = duydxi*rg_derivative_xi_y_of_hexa(m) + duydet*rg_derivative_eta_y_of_hexa(m) + duydze*rg_derivative_zeta_y_of_hexa(m)
!!$       duydz = duydxi*rg_derivative_xi_z_of_hexa(m) + duydet*rg_derivative_eta_z_of_hexa(m) + duydze*rg_derivative_zeta_z_of_hexa(m)
!!$
!!$       duzdx = duzdxi*rg_derivative_xi_x_of_hexa(m) + duzdet*rg_derivative_eta_x_of_hexa(m) + duzdze*rg_derivative_zeta_x_of_hexa(m)
!!$       duzdy = duzdxi*rg_derivative_xi_y_of_hexa(m) + duzdet*rg_derivative_eta_y_of_hexa(m) + duzdze*rg_derivative_zeta_y_of_hexa(m)
!!$       duzdz = duzdxi*rg_derivative_xi_z_of_hexa(m) + duzdet*rg_derivative_eta_z_of_hexa(m) + duzdze*rg_derivative_zeta_z_of_hexa(m)

       !
       !---------->compute elastic stress (elastic simulation) or unrelaxed elastic stress (viscoelastic simulation)

       trace_tau = (rg_rhovp2_gll(m) - 2.0*rg_rhovs2_gll(m))*(duxdx+duydy+duzdz)

       tauxx     = trace_tau + 2.0*rg_rhovs2_gll(m)*duxdx
       tauyy     = trace_tau + 2.0*rg_rhovs2_gll(m)*duydy
       tauzz     = trace_tau + 2.0*rg_rhovs2_gll(m)*duzdz

       tauxy     =                 rg_rhovs2_gll(m)*(duxdy+duydx)
       tauxz     =                 rg_rhovs2_gll(m)*(duxdz+duzdx)
       tauyz     =                 rg_rhovs2_gll(m)*(duydz+duzdy)
       !
       !---------->compute viscoelastic stress
       !---------->compute viscoelastic stress

       ! remove for now

       !---------->store members of integration of the gll node klm

! mettre mkl ici

!                           (dxidx    detdx    dzedx)
!                           (dxidy    detdy    dzedy)
!                           (dxidz    detdz    dzedz)
!
!  (tauxx   tauxy   tauxz)  (intpx1   intpx2  intpx3)
!  (tauxy   tauyy   tauyz)  (intpy1   intpy2  intpy3)
!  (tauxz   tauyz   tauzz)  (intpz1   intpz2  intpz3)


       intpx1(m) = rg_jacobian_determinant_of_hexa(m)*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz)
       intpx2(m) = rg_jacobian_determinant_of_hexa(m)*(tauxx*detdx+tauxy*detdy+tauxz*detdz)
       intpx3(m) = rg_jacobian_determinant_of_hexa(m)*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz)

       intpy1(m) = rg_jacobian_determinant_of_hexa(m)*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz)
       intpy2(m) = rg_jacobian_determinant_of_hexa(m)*(tauxy*detdx+tauyy*detdy+tauyz*detdz)
       intpy3(m) = rg_jacobian_determinant_of_hexa(m)*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz)

       intpz1(m) = rg_jacobian_determinant_of_hexa(m)*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz)
       intpz2(m) = rg_jacobian_determinant_of_hexa(m)*(tauxz*detdx+tauyz*detdy+tauzz*detdz)
       intpz3(m) = rg_jacobian_determinant_of_hexa(m)*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz)


    enddo

!DEC$ SIMD VECREMAINDER
    do m=1,n3
       tmpx1 = &
            (intpx1(n_l_k_1(m))*prod1(m,1)) +&
            (intpx1(n_l_k_2(m))*prod1(m,2)) +&
            (intpx1(n_l_k_3(m))*prod1(m,3)) +&
            (intpx1(n_l_k_4(m))*prod1(m,4)) +&
            (intpx1(n_l_k_5(m))*prod1(m,5)) 


       tmpx2 = & 
            intpx2(m_n_k_1(m))*prod2(m,1) +&
            intpx2(m_n_k_2(m))*prod2(m,2) +&
            intpx2(m_n_k_3(m))*prod2(m,3) +&
            intpx2(m_n_k_4(m))*prod2(m,4) +&
            intpx2(m_n_k_5(m))*prod2(m,5) 


       tmpx3 = &
            intpx3(m_l_n_1(m))*prod3(m,1) +&
            intpx3(m_l_n_2(m))*prod3(m,2) +&
            intpx3(m_l_n_3(m))*prod3(m,3) +&
            intpx3(m_l_n_4(m))*prod3(m,4) +&
            intpx3(m_l_n_5(m))*prod3(m,5)


       igll = ig_global_gll_of_hexa(m)

       rg_acceleration_gll_1(igll) = rg_acceleration_gll_1(igll) - & 
            (fac1(m) *tmpx1  + fac2(m) *tmpx2  + fac3(m) *tmpx3 )

enddo


!DEC$ SIMD VECREMAINDER
    do m=1,n3

       tmpy1 = &
            intpy1(n_l_k_1(m))*prod1(m,1) +&
            intpy1(n_l_k_2(m))*prod1(m,2) +&
            intpy1(n_l_k_3(m))*prod1(m,3) +&
            intpy1(n_l_k_4(m))*prod1(m,4) +&
            intpy1(n_l_k_5(m))*prod1(m,5) 

       tmpy2 = &
            intpy2(m_n_k_1(m))*prod2(m,1) +&
            intpy2(m_n_k_2(m))*prod2(m,2) +&
            intpy2(m_n_k_3(m))*prod2(m,3) +&
            intpy2(m_n_k_4(m))*prod2(m,4) +&
            intpy2(m_n_k_5(m))*prod2(m,5) 

       tmpy3 = &
            intpy3(m_l_n_1(m))*prod3(m,1) +&
            intpy3(m_l_n_2(m))*prod3(m,2) +&
            intpy3(m_l_n_3(m))*prod3(m,3) +&
            intpy3(m_l_n_4(m))*prod3(m,4) +&
            intpy3(m_l_n_5(m))*prod3(m,5)
       igll = ig_global_gll_of_hexa(m)

       rg_acceleration_gll_2(igll) = rg_acceleration_gll_2(igll) - & 
            (fac1(m) *tmpy1  + fac2(m) *tmpy2  + fac3(m) *tmpy3 )

enddo
!DEC$ SIMD VECREMAINDER
    do m=1,n3

       tmpz1 = &
            intpz1(n_l_k_1(m))*prod1(m,1) +&
            intpz1(n_l_k_2(m))*prod1(m,2) +&
            intpz1(n_l_k_3(m))*prod1(m,3) +&
            intpz1(n_l_k_4(m))*prod1(m,4) +&
            intpz1(n_l_k_5(m))*prod1(m,5)


       tmpz2 = &
            intpz2(m_n_k_1(m))*prod2(m,1) +&
            intpz2(m_n_k_2(m))*prod2(m,2) +&
            intpz2(m_n_k_3(m))*prod2(m,3) +&
            intpz2(m_n_k_4(m))*prod2(m,4) +&
            intpz2(m_n_k_5(m))*prod2(m,5) 

       tmpz3 = &
            intpz3(m_l_n_1(m))*prod3(m,1) +&
            intpz3(m_l_n_2(m))*prod3(m,2) +&
            intpz3(m_l_n_3(m))*prod3(m,3) +&
            intpz3(m_l_n_4(m))*prod3(m,4) +&
            intpz3(m_l_n_5(m))*prod3(m,5)


       igll = ig_global_gll_of_hexa(m)

       rg_acceleration_gll_3(igll) = rg_acceleration_gll_3(igll) - & 
            (fac1(m) *tmpz1  + fac2(m) *tmpz2  + fac3(m) *tmpz3 )

    enddo


    !enddo !loop on element

    return
    !******************************************************
  end subroutine compute_internal_forces_order4
     !******************************************************


     !
   end module mod_solver
